function _swall(message, timer=3000, type='success') {
    let Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: timer,
        type: type,
        title: message
    });

    Toast.fire({
        type: type,
        title: message
    })
}