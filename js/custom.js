jQuery(() => {
    $.widget.bridge('uibutton', $.ui.button)

    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop()
        $(this).next('.custom-file-label').addClass('selected').html(fileName)
    })

    $('.nav-sidebar a').filter(function() {
        return this.href == window.location.href;
    }).addClass('active');

    $('.nav-sidebar a').filter(function() {
        return this.href +'#' == window.location.href;
    }).addClass('active');

    $('.menu-master .nav a').filter(function() {
        return this.href == window.location.href;
    }).parent().parent().prev().addClass('active').parent().addClass('menu-open');

    $('.menu-master .nav a').filter(function() {
        return this.href+'*' == window.location.href;
    }).parent().parent().prev().addClass('active').parent().addClass('menu-open');

    $('.menu-master .nav a').filter(function() {
        return this.href +'#' == window.location.href;
    }).parent().parent().prev().addClass('active').parent().addClass('menu-open');

    $('.money').simpleMoneyFormat();
    $('.money').on('keypress', function (e) {
        let charCode = (e.which) ? e.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    })
    $('[type=number]').on('keydown', function (event) {
        let charCode = ('which' in event) ? event.which : event.keyCode
        if (charCode == 69 || charCode == 101) 
            return false
    })
})
