function _toast(message, type='success', timer=2500) {
    Swal.fire({
        position: 'top-end',
        icon: type,
        title: message,
        showConfirmButton: false,
        timer: timer
    })
}