<?php

Route::get('/', function () {
    return redirect()->route('reseller.shop.get');
});

/* --------- RESELLERS --------- */

// SHOP
Route::get('/', 'Resellers\ShopController@index')->name('reseller.shop.get');
Route::get('/shop', 'Resellers\ShopController@index')->name('reseller.shop.get');
Route::get('/shop/product/{id}', 'Resellers\ShopController@show')->name('reseller.shop.show.get');
Route::get('/shop/search', 'Resellers\ShopController@search')->name('reseller.shop.search.get');
Route::get('/shop/sort', 'Resellers\ShopController@sort')->name('reseller.shop.sort.get');

// AUTH
Route::group(['prefix' => 'reseller', 'middleware' => ['guest']], function () {
    Route::get('/login', 'Resellers\Auth\AuthController@getLogin')->name('reseller.login.get');
	Route::post('/login', 'Resellers\Auth\AuthController@postLogin')->name('reseller.login.post');

	Route::get('/register', 'Resellers\Auth\AuthController@getRegister')->name('reseller.register.get');
	Route::post('/register', 'Resellers\Auth\AuthController@postRegister')->name('reseller.register.post');
});


Route::group(['prefix' => 'reseller'], function() {
	// LOGOUT
	Route::get('/logout', 'Resellers\Auth\AuthController@logout')->name('reseller.logout.get');

	// DASHBOARD
	Route::get('/dashboard', 'Resellers\DashboardController@index')->name('reseller.dashboard.get');

	// ORDERS
	Route::get('/orders', 'Resellers\OrderController@index')->name('reseller.order.get');

	// ADDRESS BILLING
	Route::get('/address/billing', 'Resellers\AddressBillingController@index')->name('reseller.address.billing.get');
	Route::put('/address/billing', 'Resellers\AddressBillingController@update')->name('reseller.address.billing.update');

	// ADDRESS SHIPPING
	Route::get('/address/shipping', 'Resellers\AddressShippingController@index')->name('reseller.address.shipping.get');
	Route::put('/address/shipping', 'Resellers\AddressShippingController@update')->name('reseller.address.shipping.update');

	// DETAIL ACCOUNT
	Route::get('/profile', 'Resellers\ProfileController@index')->name('reseller.profile.get');
	Route::put('/profile', 'Resellers\ProfileController@update')->name('reseller.profile.update');
	Route::put('/profile/change-password', 'Resellers\ProfileController@changePassword')->name('reseller.profile.change.password.update');

	// CART
	Route::post('/cart', 'Resellers\CartController@store')->name('reseller.cart.post');
	Route::post('/cart/delete', 'Resellers\CartController@delete')->name('reseller.cart.delete');

	// CHECKOUT
	Route::get('/checkout', 'Resellers\CheckoutController@index')->name('reseller.checkout.get');
	Route::post('/checkout', 'Resellers\CheckoutController@store')->name('reseller.checkout.post');
	Route::get('/checkout/order-received/{id}', 'Resellers\CheckoutController@orderReceived')->name('reseller.orderReceived.get');
	Route::get('/checkout/konfirmasi-pembayaran/{id}', 'Resellers\CheckoutController@konfirmasiPembayaran')->name('reseller.konfirmasiPembayaran.get');
	Route::post('/checkout/konfirmasi-pembayaran/{id}', 'Resellers\CheckoutController@konfirmasiPembayaranPost')->name('reseller.konfirmasiPembayaran.post');
});


/* --------- ADMIN --------- */
Route::group(['prefix' => 'admin', 'namespace' => 'Auth'], function() {
	// Authentication Routes
	Route::get('/', function () {
		return redirect('admin/login');
	});
	Route::get('/login', 'LoginController@showLoginForm')->name('login');
	Route::post('/login', 'LoginController@login');
	Route::post('/logout', 'LoginController@logout')->name('logout');

	// Registration Routes...
	Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
	Route::post('/register', 'RegisterController@register');
});

Route::group(['prefix'=>'admin', 'middleware'=> ['auth', 'admin']], function() {
	
	/* --------- Dashboard --------- */
	Route::get('/home', 'HomeController@index')->name('home');

	/* --------- Order / Transaction --------- */
	Route::resource('/order', 'OrderController')->except([
		'show', 'create', 'edit'
	]);
	Route::get('/order/detail/{id}', 'OrderController@show')->name('order.detail');
	Route::get('/order/search', 'OrderController@search')->name('order.search');
	Route::get('/order/new_order', 'OrderController@create')->name('order.new_order');
	Route::get('/order/edit_order/{id}', 'OrderController@edit')->name('order.edit_order');
	Route::get('/order/canceled', 'OrderController@canceled')->name('order.canceled');

	/* --------- Product Management --------- */
	Route::resource('/product', 'ProductController')->except([
		'show', 'create', 'edit'
	]);

	Route::get('/product/detail_product/{id}', 'ProductController@show')->name('product.detail');
	Route::get('/product/add_product', 'ProductController@create')->name('product.create');
	Route::get('/product/{id}/edit_product', 'ProductController@edit')->name('product.edit');
	Route::get('/product/search', 'ProductController@search')->name('product.search');
	Route::get('/product/history/{variant_id}', 'ProductController@history_variant')->name('product.history_variant');
	
	Route::get('/product/category', 'ProductCategoryController@index')->name('product_category.index');
	Route::get('/product/category/{slug}', 'ProductCategoryController@show')->name('product_category.detail');
	Route::get('/product/category/{id}/edit', 'ProductCategoryController@edit')->name('product_category.edit');
	Route::post('/product/category/store', 'ProductCategoryController@store')->name('product_category.store');
	Route::put('/product/category/{id}', 'ProductCategoryController@update')->name('product_category.update');
	Route::delete('/product/category/{id}', 'ProductCategoryController@destroy')->name('product_category.destroy');
	
	Route::get('/product/stock/listdata', 'ProductStockController@listData')->name('product_stock.data');
	Route::get('/product/stock', 'ProductStockController@index')->name('product_stock.index');
	Route::post('/product/stock', 'ProductStockController@store')->name('product_stock.store');
	Route::put('/product/stock/{id}', 'ProductStockController@update')->name('product_stock.update');
	Route::get('/product/stock/{id}/edit', 'ProductStockController@edit')->name('product_stock.edit');
	Route::delete('/product/stock/{id}', 'ProductStockController@destroy')->name('product_stock.destroy');

	Route::get('/product/attribute/listdata', 'ProductAttributeController@listData')->name('product_attribute.data');
	Route::get('/product/attribute', 'ProductAttributeController@index')->name('product_attribute.index');
	Route::post('/product/attribute', 'ProductAttributeController@store')->name('product_attribute.store');
	Route::put('/product/attribute/{id}', 'ProductAttributeController@update')->name('product_attribute.update');
	Route::get('/product/attribute/{id}/edit', 'ProductAttributeController@edit')->name('product_attribute.edit');
	Route::delete('/product/attribute/{id}', 'ProductAttributeController@destroy')->name('product_attribute.destroy');

	/* --------- Customer --------- */
	Route::get('/customer/listdata', 'CustomerController@listData')->name('customer.data');
	Route::resource('/customer', 'CustomerController')->except([
		'show', 'create'
	]);
	Route::get('/customer/search', 'CustomerController@searchCustomer')->name('customer.search');
	// Route::get('/customer/transaction/{id}', 'TransactionController@showCustomerTransaction')->name('customer.transaction');

	/* --------- Supplier --------- */
	Route::get('/supplier/listdata', 'SupplierController@listData')->name('supplier.data');
	Route::resource('/supplier', 'SupplierController')->except([
		'show', 'create'
	]);

	Route::get('/reseller', function() {
	    return 'View Not Found!';
	});

	/* --------- Expense --------- */
	Route::get('/expense/listdata', 'ExpenseController@listdata')->name('expense.data');
	Route::resource('/expense', 'ExpenseController')->except([
		'create'
	]);
	Route::get('/expense/search', 'ExpenseController@search')->name('expense.search');

	/* --------- Report --------- */
	Route::get('/report', 'ReportController@index')->name('report.index');
	Route::get('/report/search', 'ReportController@search')->name('report.search');
	Route::get('/report/detail/{id}', 'ReportController@show')->name('report.detail');

	/* --------- Analyzer --------- */
	Route::get('/analyzer', 'AnalyzerController@index')->name('analyzer.index');
	Route::get('/analyzer/best_seller', 'AnalyzerController@bestSeller')->name('analyzer.best_seller');
	Route::get('/analyzer/best_customer', 'AnalyzerController@bestCustomer')->name('analyzer.best_customer');
	Route::get('/analyzer/best_location', 'AnalyzerController@bestLocation')->name('analyzer.best_location');

	/* --------- Setting --------- */
	Route::get('/setting/general', 'SettingController@index')->name('setting.general');
	Route::get('/setting/supplier', 'SettingController@supplier')->name('setting.supplier');
	Route::get('/setting/user', 'SettingController@user')->name('setting.user');
	Route::get('/setting/sale_target', 'SettingController@saleTarget')->name('setting.sale_target');

	/* --------- Outlet --------- */
	Route::get('setting/general/outlet/{id}/edit', 'OutletController@edit')->name('outlet.edit');
	Route::post('setting/general/outlet', 'OutletController@insert')->name('outlet.insert');
	
	/* --------- User --------- */
	Route::resource('/role', 'RoleController')->except([
        'create', 'show'
    ]);

	/* --------- User --------- */
	Route::get('/user/listdata', 'UserController@listData')->name('user.data');
	Route::resource('/user', 'UserController')->except([
		'index', 'show'
	]);
	Route::get('/user/user_profile', 'UserController@profile')->name('user.profile');
	Route::put('/user/user_profile/{id}', 'UserController@changeProfile')->name('user.update_profile');

	Route::get('/user/roles/{id}', 'UserController@roles')->name('user.roles');
    Route::put('/user/roles/{id}', 'UserController@setRole')->name('user.set_role');
    Route::post('/user/permission', 'UserController@addPermission')->name('user.add_permission');
    Route::get('/user/role_permission', 'UserController@rolePermission')->name('user.roles_permission');
    Route::put('/user/permission/{role}', 'UserController@setRolePermission')->name('user.setRolePermission');

});