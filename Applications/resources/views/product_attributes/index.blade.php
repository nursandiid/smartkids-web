@extends('layouts.master')

@section('title', 'Atribut Produk')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Produk</a></li>
    <li class="breadcrumb-item active">Atribut</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-12">
        @card
            @slot('card_id', 'card_content')

            @slot('title')
                <a href="#" class="btn btn-primary btn-sm" onclick="add()"><i class="fa fa-plus-circle"></i> Tambah</a>
            @endslot

            @table
                @slot('table_id', 'datatable')
                @slot('thead')
                    <th width="5%">#</th>
                    <th>Produk</th>
                    <th>Atribut</th>
                    <th width="60%">Terms</th>
                    <th width="10%">Aksi</th>
                @endslot

                @foreach ($attributes as $key => $attribute)
                <tr>
                    <td>{{ $key +1 }}</td>
                    <td>{{ $attribute->product['name'] }}</td>
                    <td>{{ $attribute->name }}</td>
                    <td>
                        <div class="block">
                            {{ $attribute->value }}
                        </div> 
                        <abbr title="Configure terms" data-toggle="collapse" data-target="#toggle_varian{{ $key+1 }}" aria-expanded="true" aria-controls="toggle_varian{{ $key+1 }}">
                            <span class="text-success">Configure terms</span>
                        </abbr>

                        <div class="accordion" id="accordionExample{{ $key+1 }}">
                            <div id="toggle_varian{{ $key+1 }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample{{ $key+1 }}">
                                <div class="card-body p-0 shadow-sm my-2">
                                    <table class="table bg-white table-sm table-borderless border shadow-sm">
                                        <tr>
                                            <th width="10%">VARIAN</th>
                                            <th>STOK</th>
                                            <th width="40%">HARGA JUAL</th>
                                            <th width="15%"><a href="#" class="text-success"><i class="fas fa-plus-circle"></i></a></th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" value="L" readonly>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" value="29" readonly>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" value="Rp. 90.000" readonly>
                                            </td>
                                            <td>
                                                <a href="#" class="ml-2" onclick="toggleReadonly()"><i class="fas fa-pencil-alt"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm text-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <a href="#" onclick="edit('{{ $attribute->id }}')" class="btn btn-link btn-sm"><i class="fa fa-pencil-alt"></i></a>
                        <form method="post" action="{{ route('product_attribute.destroy', $attribute->id) }}" class="d-inline-block">
                            @csrf @method('delete')
                            <button type="submit" class="btn btn-link btn-sm text-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            @endtable
        @endcard
    </div>
</div>
@include('product_attributes.form')
@include('components.sweet')
@endsection

@push('scripts')
<script src="{{ asset('/js/validate.js') }}"></script>
<script>

    let table = $('#datatable').DataTable({
        'autoWidth' : false,
        "language": {
          "emptyTable": "Ups data masih kosong!.",
          "zeroRecords": "Ups data tidak ditemukan!."
        }
    })

    $(function() {
        $('#modal-form form').on('submit', function(e) {
            id = $('#id').val();
                
            if(save_method == 'add') url = '{{ route("product_attribute.store") }}';
            else url = 'attribute/' + id;

            $('form').attr('action', url)
            if ($('#name').val() != '') $('#modal-form').modal('hide');
        })
    })

    function add() {
        save_method = 'add';
        $('.needs-validation').removeClass('was-validated')
        $('#modal-form').modal('show');
        $('#modal-form .modal-title').html('Tambah Atribut')
        $('input[name=_method]').val('POST');
        $('#modal-form form')[0].reset();
    }

    function edit(id) {
        save_method = 'edit';
        $('.needs-validation').removeClass('was-validated')
        $('input[name=_method]').val('PUT');
        $('#modal-form form')[0].reset();

        $.ajax({
            url : 'attribute/' + id + '/edit',
            type : 'GET',
            dataType : 'JSON',
            success : function(data) {
                $('#modal-form').modal('show');
                $('#modal-form .modal-title').html('Edit Atribut')

                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#product_id').val(data.product_id);
            }, 
            error : function() {
                alert('Tidak dapat menampilkan data');
            }
        })
    }
 
    function _delete(id) {
        if(confirm('Apakah yakin data akan dihapus?')) {
            $.ajax({
                url : 'attribute/' + id,
                type : 'POST',
                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data) {
                    _swall(data.message);
                },
                error : function(data) {
                    alert('Tidak dapat menghapus data!');
                }
            })
        }
    }
</script>
@endpush