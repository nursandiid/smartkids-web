@extends('layouts.master')

@section('title', 'Pengaturan')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Pengaturan</li>
    <li class="breadcrumb-item active">Toko</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-12">
        @card
			<form class="form fom-horizontal needs-validation" novalidate method="post" enctype="multipart/form-data">
				@csrf @method('post')
				<input type="hidden" name="id" id="id">
				<div class="form-group">
	                <label class="col-form-label">Logo</label>
	                <div class="col-md-6">
				    	<div class="show-logo mt-3">
				    		<img src="" class="rounded border bg-light" id="outlet_logo" style="width: 200px;">
				    	</div>
				    	
	                    <label for="logo" title="Choose File" style="cursor: pointer;"><i class="fas fa-image text-success" style="font-size: 2em;"></i></label>
	                    <input type="file" name="logo" id="logo" class="d-none d-lg-none">
				    </div>
				</div>
		        <div class="form-group">
					<label for="name" class="col-form-label">Nama Toko</label>
					<div class="col-md-6">
						<input type="text" name="name" id="name" class="form-control" required>
						<div class="invalid-feedback">
		                    Isi nama toko.
		                </div>
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-form-label">Alamat</label>
					<div class="col-md-8">
						<textarea name="address" id="address" rows="3" class="form-control" required></textarea>
						<div class="invalid-feedback">
		                    Isi alamat toko anda.
		                </div>
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-form-label">Deskripsi</label>
					<div class="col-md-8">
						<textarea name="description" id="description" rows="3" class="form-control"></textarea>
						<div class="valid-feedback">
		                    Isi penjelasan singkat tentang toko anda.
		                </div>
					</div>
				</div>
				<div class="form-group">
					<label for="phone" class="col-form-label">HP / No Telpon</label>
					<div class="col-md-4">
						<input type="text" name="phone" id="phone" class="form-control" required>
						<div class="invalid-feedback">
		                    Isi HP / No telpon.
		                </div>
					</div>
				</div>
			    <div class="form-group">
			    	<div class="col-md-2"></div>
			    	<div class="col-md-10">
			    		<button type="submit" class="btn btn-primary">Save</button>
			    		<button type="reset" class="btn btn-secondary">Reset</button>
			    	</div>
			    </div>
			</form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
	$('.form-group').addClass('row')
    $('.col-form-label').addClass('col-md-2')

	$(function() {
		showData()

		$('.form').validator().on('submit', function(e) {
			if(!e.isDefaultPrevented()) {
				$.ajax({
					url: '{{ url('admin/setting/general/outlet') }}',
					type: 'POST',
					data: new FormData($('.form')[0]),
					async: false,
					processData: false,
					contentType: false,
					success: function(data) {
						showData()
						_swall(data.message)
					},
					error: function(data) {
						alert('Tidak dapat menyimpan data!')
					}
				})
				return false;
			}
		}) 
	})

	function showData() {
		$.ajax({
			url: '{{ url('admin/setting/general/outlet') }}/1/edit',
			type: 'GET',
			dataType: 'JSON',
			success: function(data) {
				if (data != null) {
					$('#id').val(data.id)
					$('#outlet_logo').val(data.logo)
					$('#name').val(data.name)
					$('#phone').val(data.phone)
					$('#address').val(data.address)
					$('#description').val(data.description)

					d = new Date();
					$('.show-logo').html('<img src="{{ Storage::disk('public')->url('uploads/outlets/') }}'+ data.logo+'?'+d.getTime()+'" class="rounded border bg-light" style="width: 200px;">');
				}
			}, 
			error: function(data) {
				alert('Tidak dapat menampilkan data!');
			}
		})
	}
</script>
@endpush
