<div class="card card-orange card-outline" id="{{ isset($card_id) ? $card_id : '' }}">
    
    @isset($title)
    <div class="card-header">
        {{ $title }}
    </div>
    @endisset

    <div class="card-body table-responsive">
        {{ $slot }}
    </div>

    @isset($footer)
        <div class="card-footer">
	    	{{ $footer }}
	    </div>
    @endisset
    
</div>