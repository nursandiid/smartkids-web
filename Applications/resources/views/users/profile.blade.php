@extends('layouts.master')

@section('title', 'Edit Profile')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Edit Profile</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-12">
        @card
			<form class="form fom-horizontal" method="post" data-toggle="validator" enctype="multipart/form-data">
				@csrf @method('put')

				<div class="form-group">
				    <label for="name" class="col-form-label">Nama</label>
				    <div class="col-md-6">
				    	<input type="text" name="name" id="name" class="form-control" value="{{ Auth::user()->name }}" required>
				    	<span class="help-block with-errors text-danger"></span>
				    </div>
				</div>
				<div class="form-group">
	                <label class="col-form-label">Foto</label>
	                <div class="col-md-6">
				    	<div class="show-logo mt-3">
				    		<img src="{{ Storage::disk('public')->url('uploads/users/'. Auth::user()->photo) }}" class="rounded border img-profile bg-light" id="profile_logo" style="width: 200px;">
				    	</div>
				    	
	                    <label for="photo" title="Choose File" style="cursor: pointer;"><i class="fas fa-image text-success" style="font-size: 2em;"></i></label>
	                    <input type="file" name="photo" id="photo" class="d-none d-lg-none">
				    </div>
				</div>
				
				<div class="form-group">
				    <label for="old_password" class="col-form-label">Password Lama</label>
				    <div class="col-md-8">
				    	<input type="password" name="old_password" id="old_password" class="form-control">
				    	<span class="help-block with-errors"></span>
				    </div>
				</div>
				
				<div class="form-group">
					<label for="password" class="col-form-label">Password</label>
					<div class="col-md-8">
						<input type="password" name="password" id="password" class="form-control" minlength="6">
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="password1" class="col-form-label">Ulangi Password</label>
					<div class="col-md-8">
						<input type="password" name="password1" id="password1" class="form-control" data-match="#password">
						<span class="help-block with-errors"></span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-2"></div>
	                <div class="col-md-10">
	                	<button type="submit" class="btn btn-primary">Save</button>
	                	<button type="reset" class="btn btn-secondary">Reset</button>
	                </div>
	            </div>
	        </form>
        @endcard
    </div>
</div>
@endsection


@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script>
	$(function() {
		$('.form-group').addClass('row')
        $('.col-form-label').addClass('col-md-2 font-weight-normal')
        $('.form-control').addClass('form-control-sm')

		$('#old_password').keyup(function() {
			if($(this).val() != '') $('#password, #password1').attr('required', true);
			else $('#password, #password1').attr('required', false);
		})

		$('.form').validator().on('submit', function(e) {
			if(!e.isDefaultPrevented()) {
				$.ajax({
					url : '{{ route('user.update_profile', Auth::user()->id) }}',
					type : 'POST',
					data : new FormData($('.form') [0]),
					dataType : 'JSON',
					async : false,
					processData : false,
					contentType : false,
					success : function(data) {
						// tampilkan pesan jika msg=error
						if(data.msg == 'error') {
							alert('Password lama salah!');
							$('#old_password').focus().select();
						} else {
							d = new Date();
							_swall(data.message);

							// update foto user
							$('#img-profile, .img-profile').attr('src', '{{ Storage::disk('public')->url('uploads/users/') }}'+data.image+'?'+d.getTime());
							$('#name').val(data.name);
							$('.name-profile').text(data.name);
						}

						// reset
						$('#old_password, #password, #password1, #photo').val('')
					},
					error : function() {
						alert('Tidak dapat menyimpan data!');
					}
				});

				return false;
			}
		})
	})
</script>
@endpush
