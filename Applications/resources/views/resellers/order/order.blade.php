@extends('resellers.partials.layouts.index')

@section('title', 'Order')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Orders</h1>
        </div>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Tabel Order</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-md">
                                <tbody>
                                    <tr>
                                        <th>ORDER</th>
                                        <th>TANGGAL</th>
                                        <th>STATUS</th>
                                        <th>ITEM</th>
                                        <th>TOTAL</th>
                                        <th>AKSI</th>
                                    </tr>

                                    @foreach ($orders as $order)
                                        <tr>
                                            <td>{{ $order->id }}</td>
                                            <td>{{ date('d F Y', strtotime($order->created_at)) }}</td>
                                            <td>{{ $order->status }}</td>
                                            <td>{{ $order->total_item }}</td>
                                            <td>{{ $order->total_price }}</td>
                                            <td><a href="#" class="btn btn-primary">Detail</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection