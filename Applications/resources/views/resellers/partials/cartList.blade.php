@if(Session::has('cart'))
    @foreach (Session::get('cart') as $item)
        <div class="row align-items-center">
            <div class="col-9 col-md-9 col-lg-9">
                <a href="{{ route('reseller.shop.show.get', $item->product->id) }}" class="dropdown-item">
                    <img src="{{ asset('images/products/' . $item->product->photo) }}" width="60" height="60">
                    <div class="dropdown-item-desc">
                        {{ $item->product->name }}
                        <div class="time text-primary">@currency($item->product->nominal_price) X {{ $item->qty }} Item</div>
                    </div>
                </a>
            </div>

            <div id="delete" class="col-1 col-md-1 col-lg-1">
                <form action="">
                    <button type="submit" id="delete-cart" data-id="{{ $item->id }}" class="btn btn-danger">X</button>
                </form>
            </div>
        </div>
    @endforeach
    <span id="total" class="d-none">@currency(Session::get('cartSum'))</span>
@endif

@push('scripts')
<script>
    $(document).ready(function() {
        $("body").on("click", "#delete-cart", function(e){
            e.preventDefault();
            loading = true;
            
            var id = $(this).data("id");

            $.ajax({
                type:'POST',
                url: '{{ route("reseller.cart.delete") }}',
                data:{
                    _token: '{{ csrf_token() }}',
                    id_cart: id,
                },
            }).done(function(data){
                loading = false;
                console.log(data);

                $("#cart-list").html(data);
                console.log($(this).parents("dropdown-item"))

                var total = $("#total").text();
                $(".txt-total").empty().append(total);
            }).fail(function(err){
                console.log(err)
                loading = false;
                
                if(err.status == 422 || err.status == 404) {
                    alert(err.responseJSON.message);
                }else{
                    alert("Terjadi kesalahan, Reload browser anda");
                }

            });
        });
    })
</script>
@endpush