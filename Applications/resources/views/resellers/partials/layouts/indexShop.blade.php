<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Smartkids &mdash; @yield('title')</title>

    {{-- General CSS Files --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    {{-- CSS Libraries --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

    {{-- Template CSS --}}
    <link rel="stylesheet" href="{{ asset('css/resellers/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/resellers/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/resellers/shop.css') }}">
    <link rel="stylesheet" href="{{ asset('css/resellers/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/resellers/components.css') }}">

    {{-- Page Specific CSS File --}}
    @stack('styles')

    {{-- Favicon --}}
    <link rel="icon" href="{{ asset('images/favicon.jpg') }}" sizes="32x32">
</head>

<body>
    <div class="se-pre-con"></div>

    <div id="app">
        <div class="main-wrapper">
            {{-- NAVBAR --}}
            <nav class="navbar navbar-bg navbar-expand-lg main-navbar">
                <ul class="navbar-nav navbar-left">
                    <li>
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('images/logo.png') }}" style="width: 90px; object-fit: cover">
                        </a>
                    </li>
                </ul>
                <form action="{{ route('reseller.shop.search.get') }}" method="GET" class="form-inline mr-auto ml-auto">
                    <div class="search-element">
                        <input type="text" id="cari" name="q" class="form-control" placeholder="Cari produk..." aria-label="search">
                        <button class="btn btn-search" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </form>
                <ul class="navbar-nav navbar-right ml-auto">
                    <li class="dropdown dropdown-list-toggle"><a href="#" id="dropdown-cart" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                        <div class="dropdown-menu dropdown-list dropdown-menu-right">
                            <div class="dropdown-header">
                                <span class="float-left">Keranjang</span>
                                @if(Session::has('cart'))
                                    <span class="txt-total float-right text-success">Total: </span>
                                @endif
                                <div class="clearfix"></div>
                            </div>
                            <div id="cart-list" class="dropdown-list-content dropdown-list-icons">
                                {{-- @if(Session::has('cart'))
                                    @if(count(Session::get('cart')) != 0) --}}
                                        @include('resellers.partials.cartList')
                                    {{-- @else
                                        <div>
                                            <img src="{{ asset('images/reseller/emptyCart.svg') }}" alt="">
                                            <p>Keranjang masih Kosong</p>
                                        </div>
                                    @endif
                                @else
                                    <div>
                                        <img src="{{ asset('images/reseller/emptyCart.svg') }}" alt="">
                                        <p>Keranjang masih Kosong</p>
                                    </div>
                                @endif --}}
                            </div>
                            <div class="dropdown-footer text-center">
                                @if(Auth::guard('reseller')->check())
                                    <a href="{{ route('reseller.checkout.get') }}">Checkout <i class="fas fa-chevron-right"></i></a>
                                @endif
                            </div>
                        </div>
                    </li>
                    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link nav-link-lg message-toggle beep"><i class="far fa-envelope"></i></a>
                        <div class="dropdown-menu dropdown-list dropdown-menu-right">
                            <div class="dropdown-header">Messages
                                <div class="float-right">
                                    <a href="#">Mark All As Read</a>
                                </div>
                            </div>
                            <div class="dropdown-list-content dropdown-list-message">
                                <a href="#" class="dropdown-item dropdown-item-unread">
                                    <div class="dropdown-item-desc">
                                        <b>Kusnaedi</b>
                                        <p>Hello, Bro!</p>
                                        <div class="time">10 Hours Ago</div>
                                    </div>
                                </a>
                            </div>
                            <div class="dropdown-footer text-center">
                                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
                        <div class="dropdown-menu dropdown-list dropdown-menu-right">
                            <div class="dropdown-header">Notifications</div>
                            <div class="dropdown-list-content dropdown-list-icons">
                                <a href="#" class="dropdown-item dropdown-item-unread">
                                <div class="dropdown-item-icon bg-primary text-white">
                                    <i class="fas fa-code"></i>
                                </div>
                                <div class="dropdown-item-desc">
                                    Test
                                    <div class="time text-primary">2 Min Ago</div>
                                </div>
                                </a>
                            </div>
                            <div class="dropdown-footer text-center">
                                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </li>
                    @if(Auth::guard('reseller')->check())
                        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::guard('reseller')->user()->username }}</div></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="{{ route('reseller.profile.get') }}" class="dropdown-item has-icon">
                                    <i class="far fa-user"></i> Profile
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="{{ route('reseller.logout.get') }}" class="dropdown-item has-icon text-danger">
                                    <i class="fas fa-sign-out-alt"></i> Logout
                                </a>
                            </div>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('reseller.login.get') }}" class="nav-link nav-link-lg nav-link-user">
                                Login
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>

            {{-- MAIN CONTENT --}}
            <div class="main-content">
                @yield('content')
            </div>

            {{-- FOOTER --}}
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; 2020 <div class="bullet"></div> Smartkids</a>
                </div>
                <div class="footer-right">
                    1.0.0
                </div>
            </footer>
        </div>
    </div>

    {{-- General JS Scripts --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('js/resellers/stisla.js') }}"></script>

    {{-- JS Libraies --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    {{-- Template JS File --}}
    <script src="{{ asset('js/resellers/scripts.js') }}"></script>
    <script src="{{ asset('js/resellers/theme.js') }}"></script>
    <script src="{{ asset('js/resellers/toast.js') }}"></script>

    {{-- Page Specific JS File --}}
    @stack('scripts')

    {{-- This page --}}
    <script>
        $(window).on("load", function() {
            $(".se-pre-con").fadeOut("slow");
        });

        var total = $("#total").text();
        $(".txt-total").append(total);
    </script>
</body>
</html>
