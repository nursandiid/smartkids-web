@extends('resellers.partials.layouts.indexShop')

@section('title', 'Order')

@section('content')
    <section class="section mt-4">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-6 m-auto">
                <div class="card">
                    <div class="card-header">
                        <h4>Konfirmasi Pembayaran</h4>
                    </div>

                    <div class="card-body order-info">
                        <form action="{{ route('reseller.konfirmasiPembayaran.post', last(request()->segments())) }}" method="POST" enctype="multipart/form-data">
                            @csrf @method("POST")

                            <div class="form-group">
                                <label for="name">Atas nama (pengirim) *</label>
                                <input id="name" type="text" value="" class="form-control" name="name" required>
                            </div>

                            <div class="form-group">
                                <label for="amount">Jumlah nominal transfer *</label>
                                <input id="amount" type="text" value="" class="form-control" name="amount" required>
                            </div>

                            <div class="form-group">
                                <label for="first_name">Bank *</label>
                                <select name="bank" class="form-control">
                                    <option value="BCA">BCA</option>
                                    <option value="MANDIRI">MANDIRI</option>
                                    <option value="BRI SYARIAH">BRI SYARIAH</option>
                                    <option value="BNI SYARIAH">BNI SYARIAH</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="photo">Upload bukti pembayaran *</label>
                                <input id="photo" type="file" value="" class="form-control" name="photo" required>
                            </div>

                            <button type="submit" class="btn btn-primary btn-block">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection