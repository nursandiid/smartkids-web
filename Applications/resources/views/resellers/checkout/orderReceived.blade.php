@extends('resellers.partials.layouts.indexShop')

@section('title', 'Order')

@section('content')
    <section class="section mt-4">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Order Diterima</h4>
                    </div>

                    <div class="card-body order-info">
                        <table class="table table-bordered">
                            <tr>
                                <th>Nomor Order</th>
                                <th>Tanggal</th>
                                <th>Email</th>
                                <th>Total</th>
                            </tr>
                            <tr>
                                <td><strong>{{ $order->id }}</strong></td>
                                <td><strong>{{ $date }}</strong></td>
                                <td><strong>{{ $order->reseller->email }}</strong></td>
                                <td><strong>@currency($order->total_price)</strong></td>
                            </tr>
                        </table>    
                    </div>

                    <div class="card-footer text-right">
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4>Bank</h4>
                    </div>

                    <div class="card-body bank-details">
                        <table class="table table-bordered mb-4">
                            <tr>
                                <th>Prihatin Ekowati:</th>
                                <th>Prihatin Ekowati:</th>
                            </tr>
                            <tr>
                                <td>
                                    <span>Bank:</span> <br>
                                    <strong>BCA</strong>
                                </td>
                                <td>
                                    <span>Bank:</span> <br>
                                    <strong>Mandiri</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>No. Rekening:</span> <br>
                                    <strong>8030219164</strong>
                                </td>
                                <td>
                                    <span>No. Rekening:</span> <br>
                                    <strong>9000007119770</strong>
                                </td>
                            </tr>
                        </table>
                        
                        <table class="table table-bordered">
                            <tr>
                                <th>Prihatin Ekowati:</th>
                                <th>Prihatin Ekowati:</th>
                            </tr>
                            <tr>
                                <td>
                                    <span>Bank:</span> <br>
                                    <strong>BRI SYARIAH</strong>
                                </td>
                                <td>
                                    <span>Bank:</span> <br>
                                    <strong>BNI SYARIAH</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>No. Rekening:</span> <br>
                                    <strong>1018937658</strong>
                                </td>
                                <td>
                                    <span>No. Rekening:</span> <br>
                                    <strong>0284547005</strong>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="card-footer text-center">
                        <a href="{{ route('reseller.konfirmasiPembayaran.get', $order->id) }}" class="btn btn-primary">Konfirmasi Pembayaran</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection