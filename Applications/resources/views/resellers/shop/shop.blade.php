@extends('resellers.partials.layouts.indexShop')

@section('title', 'Toko')

@section('content')
    <section class="section mt-3">
        <div class="row">
            @if(Auth::guard('reseller')->check())
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="card card-primary box-profile">
                        <div class="card-header box-profile-header">
                            <h4>
                                <a href="{{ route('reseller.profile.get') }}">{{ Auth::guard('reseller')->user()->first_name . " " . Auth::guard('reseller')->user()->last_name }}</a>
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="mb-4">
                                <span><strong>Saldo</strong></span>
                                <div class="d-flex justify-content-between">
                                    <span>Sisa Saldo</span>
                                    <span>Rp. 0</span>
                                </div>
                            </div>

                            <div>
                                <span><strong>Points</strong></span>
                                <div class="d-flex justify-content-between">
                                    <span>Points</span>
                                    <span>0 Points</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-9 col-lg-9">
                    <div class="row">
                        <div id="carouselExampleCaptions" class="carousel slide mb-4" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{ asset('images/slide/banner.jpg') }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>First slide label</h5>
                                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('images/slide/banner.jpg') }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Second slide label</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('images/slide/banner.jpg') }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                    <div id="load" style="position: relative;" class="row list-products">
                        @if (count($products) > 0)
                            @include('resellers.partials.paginate')
                        @else
                            <h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>
                        @endif
                    </div>
                </div>
            @else
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="card card-primary box-profile">
                        <div class="card-header box-profile-header">
                            <h4>Filter</h4>
                        </div>
                        <div class="card-body">
                            <form action="" method="GET">
                                <div class="form-group">
                                    <label for="">Kategori</label>
                                    <select id="sort-category" name="sort-category" class="custom-select">
                                        <option value="0">Semua</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>

                            <form action="" method="GET">
                                <div class="form-group">
                                    <label for="">Sortir</label>
                                    <select id="sort-data" name="sort-data" class="custom-select">
                                        <option value="1">Produk terbaru</option>
                                        <option value="2">Produk terlama</option>
                                        <option value="3">Urutkan dari yang termurah</option>
                                        <option value="4">Urutkan dari yang termahal</option>
                                        <option value="5">Urutkan dari A-Z</option>
                                        <option value="6">Urutkan dari Z-A</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-9 col-lg-9">
                    <div class="row">
                        <div id="carouselExampleCaptions" class="carousel slide mb-4" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{ asset('images/slide/banner.jpg') }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>First slide label</h5>
                                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('images/slide/banner.jpg') }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Second slide label</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('images/slide/banner.jpg') }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                    <div id="load" style="position: relative;" class="row list-products">
                        @if (count($products) > 0)
                            @include('resellers.partials.paginate')
                        @else
                            <h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>
                        @endif
                    </div>
                </div>
            @endif

            <div id="scroll-up">
                <i class="fa fa-arrow-up" aria-hidden="true"></i>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        // SORT
        function sort() {
            var selectedData = $("select#sort-data").children("option:selected").val();
            var selectedCategory = $('select#sort-category').children("option:selected").val();

            $('.list-products').html('<h2 class="pt-4 m-auto">Loading...</h2>');
            
            $.ajax({
                type:'GET',
                url: '{{ route("reseller.shop.sort.get") }}',
                data:{
                    _token: '{{ csrf_token() }}',
                    key: selectedData,
                    id: selectedCategory
                },
            }).done(function(data){
                console.log(data);
                if(data != "") {
                    $('.list-products').html(data);
                }else{
                    $('.list-products').html('<h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>');
                }
            }).fail(function(err){
                alert('Produk tidak dapat di load. Silahkan melakukan reload pada browser.');
            });
        }

        $("#sort-data, #sort-category").on("change", function() {
            sort();
        });

        // PAGINATION
        $("body").on("click", "#load a.page-link", function(e){
            e.preventDefault();
            $('.list-products').html('<h2 class="pt-4 m-auto">Loading...</h2>');

            var url = $(this).attr('href');
            getProducts(url);
            window.history.pushState("", "", url);

        });

        function getProducts(url) {
            $.ajax({
                url : url  
            }).done(function (data) {
                $('.list-products').html(data).removeAttr("style");
                $(document).scrollTop($(document).height());
            }).fail(function () {
                alert('Produk tidak dapat di load. Silahkan melakukan reload pada browser.');
            });
        }

        // SEARCH
        $(".btn-search").on("click", function(e){
            e.preventDefault();
            var keyword = $("input[name='q']").val();

            var form_action = $("form.form-search").attr("action");
            $('.list-products').html('<h2 class="pt-4 m-auto">Loading...</h2>');

            $.ajax({
                type:'GET',
                url: form_action,
                data:{
                    _token: '{{ csrf_token() }}',
                    q: keyword
                },
            }).done(function(data){
                if(data != "") {
                    $('.list-products').html(data);
                }else{
                    $('.list-products').html('<h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>');
                }
            }).fail(function(err){
                alert('Produk tidak dapat di load. Silahkan melakukan reload pada browser.');
            });
        })
    })
</script>
@endpush