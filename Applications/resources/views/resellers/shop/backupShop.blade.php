@extends('resellers.partials.layouts.index')

@section('title', 'Toko')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Toko</h1>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <form action="{{ route('reseller.shop.search.get') }}" method="GET" class="form-search">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" id="cari" name="q" class="form-control" placeholder="Cari produk..." aria-label="">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-search" type="submit">Cari</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 col-md-6 col-lg-6">
                        <form action="" method="GET">
                            <div class="form-group">
                                <label for="sort-category">Kategori</label>
                                <select id="sort-category" name="sort-category" class="custom-select">
                                    <option value="0">Semua</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>

                    <div class="col-6 col-md-6 col-lg-6">
                        <form action="" method="GET">
                            <div class="form-group">
                                <label for="sort-data">Urutkan</label>
                                <select id="sort-data" name="sort-data" class="custom-select">
                                    <option value="1">Produk terbaru</option>
                                    <option value="2">Produk terlama</option>
                                    <option value="3">Urutkan dari yang termurah</option>
                                    <option value="4">Urutkan dari yang termahal</option>
                                    <option value="5">Urutkan dari A-Z</option>
                                    <option value="6">Urutkan dari Z-A</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="load" style="position: relative;" class="row list-products">
                    @if (count($products) > 0)
                        @include('resellers.partials.paginate')
                    @else
                        <h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>
                    @endif
                </div>
            </div>

            <div id="scroll-up">
                <i class="fa fa-arrow-up" aria-hidden="true"></i>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        // SORT
        function sort() {
            var selectedData = $("select#sort-data").children("option:selected").val();
            var selectedCategory = $('select#sort-category').children("option:selected").val();

            $('.list-products').html('<h2 class="pt-4 m-auto">Loading...</h2>');
            
            $.ajax({
                type:'GET',
                url: '{{ route("reseller.shop.sort.get") }}',
                data:{
                    _token: '{{ csrf_token() }}',
                    key: selectedData,
                    id: selectedCategory
                },
            }).done(function(data){
                console.log(data);
                if(data != "") {
                    $('.list-products').html(data);
                }else{
                    $('.list-products').html('<h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>');
                }
            }).fail(function(err){
                alert('Produk tidak dapat di load. Silahkan melakukan reload pada browser.');
            });
        }

        $("#sort-data, #sort-category").on("change", function() {
            sort();
        });

        // PAGINATION
        $("body").on("click", "#load a.page-link", function(e){
            e.preventDefault();
            $('.list-products').html('<h2 class="pt-4 m-auto">Loading...</h2>');

            var url = $(this).attr('href');
            getProducts(url);
            window.history.pushState("", "", url);

        });

        function getProducts(url) {
            $.ajax({
                url : url  
            }).done(function (data) {
                $('.list-products').html(data).removeAttr("style");
                $(document).scrollTop($(document).height());
            }).fail(function () {
                alert('Produk tidak dapat di load. Silahkan melakukan reload pada browser.');
            });
        }

        // SEARCH
        $(".btn-search").on("click", function(e){
            e.preventDefault();
            var keyword = $("input[name='q']").val();

            var form_action = $("form.form-search").attr("action");
            $('.list-products').html('<h2 class="pt-4 m-auto">Loading...</h2>');

            $.ajax({
                type:'GET',
                url: form_action,
                data:{
                    _token: '{{ csrf_token() }}',
                    q: keyword
                },
            }).done(function(data){
                if(data != "") {
                    $('.list-products').html(data);
                }else{
                    $('.list-products').html('<h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>');
                }
            }).fail(function(err){
                alert('Produk tidak dapat di load. Silahkan melakukan reload pada browser.');
            });
        })
    })
</script>
@endpush