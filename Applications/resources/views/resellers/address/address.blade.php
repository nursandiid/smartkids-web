@extends('resellers.partials.layouts.index')

@section('title', 'Alamat')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Alamat</h1>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Alamat</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3">
                                <ul class="nav nav-pills flex-column" id="tab">
                                    <li class="nav-item">
                                        <a href="{{ route('reseller.address.billing.get') }}" class="nav-link active">Penagihan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('reseller.address.shipping.get') }}" class="nav-link">Pengiriman</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-12 col-md-9">
                                <form action="{{ route('reseller.address.billing.update') }}" method="post" class="needs-validation">
                                    @csrf @method('PUT')

                                    <div class="row mt-3">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label for="first_name">Nama depan *</label>
                                            <input id="first_name" type="text" value="{{ $billing_address->first_name }}" class="form-control" name="first_name" required>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label for="last_name">Nama belakang *</label>
                                            <input id="last_name" type="text" value="{{ $billing_address->last_name }}" class="form-control" name="last_name">
                                        </div>
                                    </div>
                
                                    <div class="form-group">
                                        <label for="company_name">Nama perusahaan (optional)</label>
                                        <input id="company_name" type="text" value="{{ $billing_address->company_name }}" class="form-control" name="company_name">
                                    </div>

                                    <div class="form-group form-group-billing">
                                        <label for="country">Negara *</label>
                                        <select id="country" class="form-control billing-country" name="country" style="width: 100%">
                                            <option value="">Pilih negara</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Alamat *</label>
                                        <textarea name="street_address" class="form-control" id="address" cols="30" rows="100" required>{{ $billing_address->street_address }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="apartment">Apartment, suite, unit, etc. (optional)</label>
                                        <input id="apartment" type="text" value="{{ $billing_address->apartement }}" class="form-control" name="apartment">
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-12 col-md-4 col-lg-4">
                                            <label for="city">Kota *</label>
                                            <input id="city" type="text" value="{{ $billing_address->city }}" class="form-control" name="city" required>
                                        </div>
                                        <div class="form-group col-12 col-md-4 col-lg-4">
                                            <label for="province">Provinsi *</label>
                                            <input id="province" type="text" value="{{ $billing_address->province }}" class="form-control" name="province" required>
                                        </div>
                                        <div class="form-group col-12 col-md-4 col-lg-4">
                                            <label for="postal_code">Postal Code *</label>
                                            <input id="postal_code" type="number" value="{{ $billing_address->postal_code }}" class="form-control" name="postal_code" required>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label for="phone">No. Telepon *</label>
                                            <input id="phone" type="number" value="{{ $billing_address->phone }}" class="form-control" name="phone" required>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label for="email">Email *</label>
                                            <input id="email" type="email" value="{{ $billing_address->email }}" class="form-control" name="email" required>
                                        </div>
                                    </div>

                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary btn-billing">Simpan Perubahan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script>
$(document).ready(function() {
    $('select.billing-country').select2();

    var _url = 'https://restcountries.eu/rest/v2/all?fields=name';
    $.ajax({
        dataType: 'json',
        type:'GET',
        url: _url,
    }).done(function(data){
        $.each(data, function (index, data) {
            var countries = $("select.billing-country")
            countries.append($('<option value="'+ data.name +'"'+ (data.name == '{{ $billing_address->country }}' ? 'selected' : '') +'>'+ data.name +'</option>'));
        });
    }).fail(function(err){
        
    });

    $(".btn-billing").click(function(e){
        e.preventDefault();
        var form_action = $("form").attr("action");

        // data form
        var first_name = $("input[name='first_name']").val();
        var last_name = $("input[name='last_name']").val();
        var company_name = $("input[name='company_name']").val();
        var country = $("select[name='country']").val();
        var street_address = $("textarea[name='street_address']").val();
        var apartment = $("input[name='apartment']").val();
        var city = $("input[name='city']").val();
        var province = $("input[name='province']").val();
        var postal_code = $("input[name='postal_code']").val();
        var phone = $("input[name='phone']").val();
        var email = $("input[name='email']").val();

        // reset
        $("input[name='first_name']").removeClass('is-invalid')
        $("input[name='last_name']").removeClass('is-invalid')
        $("input[name='company_name']").removeClass('is-invalid')
        $("select[name='country']").removeClass('is-invalid')
        $("textarea[name='street_address']").removeClass('is-invalid')
        $("input[name='apartment']").removeClass('is-invalid')
        $("input[name='city']").removeClass('is-invalid')
        $("input[name='province']").removeClass('is-invalid')
        $("input[name='postal_code']").removeClass('is-invalid')
        $("input[name='phone']").removeClass('is-invalid')
        $("input[name='email']").removeClass('is-invalid')

        $(".err-first_name").remove();
        $(".err-last_name").remove();
        $(".err-company_name").remove();
        $(".err-country").remove();
        $(".err-street_address").remove();
        $(".err-apartment").remove();
        $(".err-city").remove();
        $(".err-province").remove();
        $(".err-postal_code").remove();
        $(".err-phone").remove();
        $(".err-email").remove();

        $.ajax({
            dataType: 'json',
            type:'PUT',
            url: form_action,
            data:{
                _token: '{{ csrf_token() }}',
                first_name: first_name,
                last_name: last_name,
                company_name: company_name,
                country: country,
                street_address: street_address,
                apartement: apartment,
                city: city,
                province: province,
                postal_code: postal_code,
                phone: phone,
                email: email
            },
        }).done(function(data){
            _toast('Alamat pengiriman berhasil diperbaharui')
        }).fail(function(err){
            _toast('Terjadi kesalahan, periksa kembali form anda', 'warning', 2000)

            if(err.responseJSON['street_address'] != undefined) {
                var street = $("textarea[name='street_address']")
                street.addClass('is-invalid');
                street.after($('<div class="err-street_address invalid-feedback">' + err.responseJSON['street_address'][0] + '</div>'));
            }
            if(err.responseJSON['country'] != undefined){
                var countries = $("select[name='country']")
                countries.addClass('is-invalid');
                countries.after($('<div class="err-country invalid-feedback">' + err.responseJSON['country'][0] + '</div>'));
            }

            $.each(err.responseJSON, function (i, error) {
                var el = $("input[name='"+ i +"']");

                el.addClass('is-invalid');
                el.after($('<div class="err-' + i + ' invalid-feedback">' + error[0] + '</div>'));
            });
        });
    });
});
</script>
@endpush