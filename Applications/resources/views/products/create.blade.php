@extends('layouts.master')

@section('title', 'Produk')

@push('css')
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/summernote/summernote-bs4.css') }}">
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <style>
        .note-btn-group.btn-group.note-insert {
            display: none;
        }

        .custom-control-label {
            cursor: pointer;
            font-weight: normal!important;
        }

        .col-md-3 .nav-pills .nav-link {
            padding: .3rem 1rem;
        }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ url('/products') }}">Produk</a></li>
    <li class="breadcrumb-item active">Tambah</li>
@endsection

@section('main-content')
<form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
<div class="row">
    <div class="col-lg-8 col-12">
        @card
            @csrf
            <div class="form-group row">
                <div class="col-12">
                    <label for="name">Nama Produk</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="description">Deskripsi</label>
                <textarea name="description" id="description" rows="3" class="form-control summernote"></textarea>
            </div>
            <div class="form-group">
                <div class="col-12">
                    <strong>Detail Produk</strong>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-3">
                    <div class="nav rounded border p-2 flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-general" role="tab" aria-controls="v-pills-general" aria-selected="true">General</a>
                        <a class="nav-link" id="v-pills-inventori-tab" data-toggle="pill" href="#v-pills-inventori" role="tab" aria-controls="v-pills-inventori" aria-selected="false">Inventori</a>
                        <a class="nav-link" id="v-pills-pengiriman-tab" data-toggle="pill" href="#v-pills-pengiriman" role="tab" aria-controls="v-pills-pengiriman" aria-selected="false">Pengiriman</a>
                        <a class="nav-link" id="v-pills-atribut-tab" data-toggle="pill" href="#v-pills-atribut" role="tab" aria-controls="v-pills-atribut" aria-selected="false">Atribut</a>
                    </div>
                </div>

                <div class="col-md-9 mt-3 mt-md-0 mt-lg-0">
                    <div class="tab-content border rounded p-2" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-general" role="tabpanel" aria-labelledby="v-pills-general-tab">
                            <div class="form-group row">
                                <label for="normal_price" class="col-sm-4 col-form-label">Harga Normal</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control money" id="normal_price" name="normal_price" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="salling_price" class="col-sm-4 col-form-label">Harga Jual</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control money" id="salling_price" name="salling_price" required>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-inventori" role="tabpanel" aria-labelledby="v-pills-inventori-tab">
                            <div class="form-group row">
                                <label for="sku" class="col-sm-4 col-form-label">SKU</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="sku" name="sku" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="manage_stock" class="col-sm-4 col-form-label">Kelola Stok?</label>
                                <div class="col-sm-8">
                                    <input type="checkbox" id="manage_stock" name="manage_stock" value="1" checked>
                                    <label class="d-inline font-weight-light" for="manage_stock">Aktifkan manajemen stok di level produk.</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="amount" class="col-sm-4 col-form-label">Jumlah Stok</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="amount" name="amount" value="0" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="allow_backorder" class="col-sm-4 col-form-label">Izinkan Backorder?</label>
                                <div class="col-sm-8">
                                    <select name="allow_backorder" id="allow_backorder" name="allow_backorder" class="custom-select custom-select-sm">
                                        <option value="Tidak diizinkan">Tidak diizinkan</option>
                                        <option value="Diizinkan, tetapi beritahu customer">Diizinkan, tetapi beritahu customer</option>
                                        <option value="Diizinkan">Diizinkan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="limit_stock" class="col-sm-4 col-form-label">Batas</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="limit_stock" name="limit_stock" value="0" required>
                                    <small>Ambang batas, stok tinggal sedikit</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sold_individually" class="col-sm-4 col-form-label">Dijual individual</label>
                                <div class="col-sm-8">
                                    <input type="checkbox" id="sold_individually" name="sold_individually" value="1" checked>
                                    <label class="d-inline font-weight-light" for="sold_individually">Jika diaktifkan produk hanya bisa dibeli 1 dalam sekali order.</label>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-pengiriman" role="tabpanel" aria-labelledby="v-pills-pengiriman-tab">
                            <div class="form-group row">
                                <label for="weight" class="col-sm-4 col-form-label">Berat (kg)</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="weight" name="weight">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="length" class="col-sm-4 col-form-label">Dimensi (cm)</label>
                                <div class="col-sm-8">
                                    <div class="form-inline">
                                        <input type="number" class="form-control col-3 mx-1" id="length" name="length" placeholder="Length">
                                        <input type="number" class="form-control col-3 mx-1" id="width" name="width" placeholder="Width">
                                        <input type="number" class="form-control col-3 mx-1" id="height" name="height" placeholder="Height">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="shipping_class" class="col-sm-4 col-form-label">Kelas Pengiriman</label>
                                <div class="col-sm-8">
                                    <select name="shipping_class" id="shipping_class" class="custom-select custom-select-sm">
                                        <option value="Tidak Ada">Tidak ada kelas pengiriman.</option>
                                        <option value="JNE">JNE</option>
                                        <option value="POS Indonesia">POS Indonesia</option>
                                        <option value="J&T">J&T</option>
                                        <option value="SiCepat">SiCepat</option>
                                        <option value="Ninja Express">Ninja Express</option>
                                        <option value="Tiki">Tiki</option>
                                        <option value="First Logistics">First Logistics</option>
                                        <option value="Indah Logistik">Indah Logistik</option>
                                        <option value="Pandu Logistik">Pandu Logistik</option>
                                        <option value="RPX">RPX</option>
                                        <option value="Cahaya Logistik">Cahaya Logistik</option>
                                        <option value="GO-SEND dari GO-JEK">GO-SEND dari GO-JEK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-atribut" role="tabpanel" aria-labelledby="v-pills-atribut-tab">
                            <div class="form-group row">
                                <div class="col-8">
                                    <select name="atribut_kusus" id="atribut_kusus" class="custom-select custom-select-sm">
                                        <option value="">Atribut Produk Khusus</option>
                                        @foreach ($product_attributes as $attribute)
                                            <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4">
                                    <button class="btn btn-outline-primary">Tambah</button>
                                </div>
                            </div>

                            <div class="form-group row border-top">
                                <div class="col-md-12 mt-2">
                                    <input type="text" class="form-control" name="attribut_name" id="attribut_name" placeholder="Nama Atribut">
                                    <div class="card-body bg-light p-1 shadow-sm my-2 rounded border">
                                        <table class="table table-sm table-borderless rounded">
                                            <tr>
                                                <th width="10%">VARIAN</th>
                                                <th>STOK</th>
                                                <th width="40%">HARGA JUAL</th>
                                                <th width="10%" class="text-center"><a href="#" class="text-success"><i class="fas fa-plus-circle"></i></a></th>
                                            </tr>
                                            <tr>
                                                <td><input type="text" class="form-control" value="L"></td>
                                                <td><input type="text" class="form-control" value="29"></td>
                                                <td><input type="text" class="form-control" value="Rp. 90.000"></td>
                                                <td class="text-center">
                                                    <button type="submit" class="btn btn-link btn-sm text-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" class="form-control" value="L"></td>
                                                <td><input type="text" class="form-control" value="29"></td>
                                                <td><input type="text" class="form-control" value="Rp. 90.000"></td>
                                                <td class="text-center">
                                                    <button type="submit" class="btn btn-link btn-sm text-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <button class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group mt-3">
                <div class="col-12">
                    <strong>Reseller Setting</strong>
                </div>
            </div>

            <table class="table table-sm table-responsive table-bordered ">
                <thead>
                    <td class="border-top">Group</td>
                    <td class="border-top">Komisi Cash</td>
                    <td class="border-top">Komisi Kredit</td>
                    <td class="border-top">Point</td>
                    <td class="border-top">
                        <button class="btn btn-link text-primary btn-sm" title="Tambah Baris"><i class="fas fa-plus-circle"></i></button>
                    </td>
                </thead>

                <tbody>
                    <td width="25%">
                        <select name="group" id="group" class="custom-select custom-select-sm">
                            <option>-- Choose -- </option>
                            <option value="Gold">Gold</option>
                            <option value="EPC">EPC</option>
                            <option value="Premium">Premium</option>
                        </select>
                    </td>
                    <td><input type="text" class="form-control" name="purchase_price" id="purchase_price"></td>
                    <td><input type="text" class="form-control" name="nominal_price" id="nominal_price"></td>
                    <td><input type="text" class="form-control" name="size" id="size"></td>
                    <td>
                        <button type="submit" class="btn btn-link btn-sm text-danger" title="Hapus Baris" onclick="return confirm('Are you sure?')"><i class="fas fa-times-circle"></i></button>
                    </td>
                </tbody>
            </table>

            <div class="form-group row mt-3">
                <div class="col-12">
                    <label for="dp">DP</label>
                    <input type="number" class="form-control money" name="dp" id="dp">
                </div>
            </div>
        @endcard
    </div>

    <div class="col-lg-4 col-12">
        @card
            <a href="{{ route('product.index') }}" class="btn btn-orange">Kembali</a>
            <button class="btn btn-success float-right" onclick="$('.submit_form').submit()">Terbitkan</button>
            <button class="btn btn-success float-right d-none">Perbarui</button>
        @endcard

        @card
            @slot('card_id', 'card_category')
                <div class="form-group">
                    <label for="product_category_id">Kategori Produk</label>
                    @foreach ($product_categories as $category)
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="{{ $category->name }}" name="product_categories[]" value="{{ $category->id }}">
                            <label class="custom-control-label" for="{{ $category->name }}">{{ $category->name }}</label>
                        </div>
                    @endforeach
                    
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="uncategorized" value="0">
                        <label class="custom-control-label" for="uncategorized">Uncategorized</label>
                    </div>

                    <!-- / Category -->

                    <div class="form-group row mt-3">
                        <div class="col-8">
                            <input type="text" class="form-control" id="category_name" name="category_name">
                        </div>
                        <div class="col-4">
                            <button type="button" class="btn btn-outline-primary" onclick="addCategory()">Tambah Baru</button>
                        </div>
                    </div>
                </div>
        @endcard

        @card
            <div class="form-group">
                <label for="foto_produk">Foto Produk</label>
                <div class="row">
                    <div class="col">
                        <img src="https://via.placeholder.com/200.png" alt="" class="img-fluid rounded mb-2">
                        <input type="file" name="photo" class="form-control form-control-sm" style="width: 50%;" id="foto_produk">

                        <a href="#" class="show_product_gallery d-block mt-3">
                            <u>Pilih gambar dari galeri produk</u>
                        </a>
                    </div>
                </div>
            </div>
        @endcard
    </div>
</div>
</form>
@include('components.sweet')
@endsection

@push('scripts')
    <script src="{{ asset('/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>

        $(function () {
            $('.summernote').summernote({
                'height': 150,
            })

            $('.select2').select2()
            $('.menu-master.produk').addClass('menu-open').children().addClass('active')
        })
    </script>
@endpush