@extends('layouts.master')

@section('title', 'Produk')

@push('css')
    <style>
        .custom-control-label {
            cursor: pointer;
        }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Produk</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-6 col-12">
        <div class="form-group row">
            <div class="col-4">
                <select name="product_category" id="product_category" class="custom-select custom-select-sm">
                    <option value="all_categories">Semua Kategori</option>

                    @foreach ($product_categories as $category)
                        <option value="{{ strtolower($category->name) }}">{{ $category->name }}</option>
                    @endforeach

                    <option value="uncategorized">Uncategorized</option>
                    <option value="archived">Archived</option>
                </select>
            </div>
            
            <div class="col-6">
                <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Masukan Nama produk">
            </div>
            
            <div class="col-2">
                <button class="btn btn-outline-primary">Filter</button>
            </div>
        </div>
    </div>
    
    <div class="col-md-4 col-12"></div>
    <div class="col-md-2 col-12">
        <div class="form-group">
            <select name="sort_by" id="sort_by" class="custom-select custom-select-sm">
                <option value="latest">Produk Terbaru</option>
                <option value="oldest">Produk Terlama</option>
                <option value="a_to_z">Urutkan A-Z</option>
                <option value="z_to_a">Urutkan Z-A</option>
                <option value="lowest">Harga Termurah - Termahal</option>
                <option value="most_expensive">Harga Termahal - Termurah</option>
                <option value="smallest">Stok Paling Sedikit</option>
                <option value="biggest">Stok Paling Banyak</option>
            </select>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="card mt-3 card-orange card-outline">
            <div class="card-header">
                <a href="{{ route('product.create') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Tambah</a>
                <a href="#" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Hapus</a>
                <a href="{{ route('product.index') }}" class="btn btn-primary"><i class="fas fa-sync"></i> Refresh</a>
                <a onclick="exportPDF()" href="#" class="btn btn-sm btn-default"><i class="fas fa-file-pdf"></i> Export PDF</a>
                <a onclick="exportExcel()" href="#" class="btn btn-sm btn-default"><i class="fas fa-file-excel"></i> Export Excel</a>
                <a onclick="importExcel()" href="#" class="btn btn-sm btn-default"><i class="fas fa-file-excel"></i> Import Excel</a>
            </div>

            <div class="card-body table-responsive">
                @table
                    @slot('thead')
                        <th width="3%">
                            <div class="custom-control custom-checkbox" style="margin-right: -1rem;">
                                <input type="checkbox" class="custom-control-input" id="select-all">
                                <label class="custom-control-label" for="select-all"></label>
                            </div>
                        </th>
                        <th class="text-center" width="10%"><i class="fas fa-image"></i></th>
                        <th>Nama</th>
                        <th>SKU</th>
                        <th>Stok</th>
                        <th>Harga</th>
                        <th>Kategori</th>
                        <th>Tanggal</th>
                    @endslot
                        
                    <body>
                        
                        @if (isset($slug))
                            @foreach ($products->products as $key => $product)
                            <tr>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck{{ $key+1 }}">
                                        <label class="custom-control-label" for="customCheck{{ $key+1 }}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    @if ($product->photo)
                                        <img src="{{ Storage::disk('public')->url('uploads/products/'. $product->photo) }}" alt="{{ $product->photo }}" width="50" class="rounded m-1">
                                    @else
                                        <img src="https://via.placeholder.com/50.png" alt="img-product" width="50" class="rounded m-1">
                                    @endif
                                </td>
                                <td class="name_product">
                                    <a href="{{ route('product.detail', $product->id) }}" class="font-weight-bold text-uppercase">{{ $product->name }}</a>

                                    <div class="d-none more-action">
                                        <a href="{{ route('product.detail', $product->id) }}" class="btn btn-link text-secondary" title="Lihat"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('product.edit', $product->id) }}" class="btn btn-link text-secondary" title="Edit"><i class="fa fa-pencil-alt"></i></a>
                                        <form action="{{ route('product.destroy', $product->id) }}" method="post" class="d-inline-block">
                                            @csrf @method('delete')
                                            <button class="btn btn-link text-danger" onclick="return confirm('Are you sure?')" title="Hapus"><i class="fa fa-trash-alt"></i></button>
                                        </form>
                                    </div>
                                </td>
                                <td>{{ !empty($product->sku) ? $product->sku : '-' }}</td>
                                <td><span class="text-success"></span>{{ $product->amount }}</td>
                                <td>{{ format_uang($product->salling_price) }}</td>
                                <td>
                                    @foreach ($product->product_categories as $category_product)
                                        <a href="{{ route('product_category.detail', $category_product->slug) }}" class="badge badge-primary">{{ $category_product->name }}</a>
                                    @endforeach
                                </td>
                                <td title="{{ !empty($product->created_at) ? tanggal_indonesia($product->created_at) : '-' }}">
                                    {{ !empty($product->created_at) ? date('d/m/Y', strtotime($product->created_at)) : '-' }}
                                </td>
                            </tr>
                            @endforeach
                        @else                        
                            @foreach ($products as $key => $product)
                            <tr>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck{{ $key+1 }}">
                                        <label class="custom-control-label" for="customCheck{{ $key+1 }}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    @if ($product->photo)
                                        <img src="{{ Storage::disk('public')->url('uploads/products/'. $product->photo) }}" alt="{{ $product->photo }}" width="50" class="rounded m-1">
                                    @else
                                        <img src="https://via.placeholder.com/50.png" alt="img-product" width="50" class="rounded m-1">
                                    @endif
                                </td>
                                <td class="name_product">
                                    <a href="{{ route('product.detail', $product->id) }}" class="font-weight-bold text-uppercase">{{ $product->name }}</a>

                                    <div class="d-none more-action">
                                        <a href="{{ route('product.detail', $product->id) }}" class="btn btn-link text-secondary" title="Lihat"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('product.edit', $product->id) }}" class="btn btn-link text-secondary" title="Edit"><i class="fa fa-pencil-alt"></i></a>
                                        <form action="{{ route('product.destroy', $product->id) }}" method="post" class="d-inline-block">
                                            @csrf @method('delete')
                                            <button class="btn btn-link text-danger" onclick="return confirm('Are you sure?')" title="Hapus"><i class="fa fa-trash-alt"></i></button>
                                        </form>
                                    </div>
                                </td>
                                <td>{{ !empty($product->sku) ? $product->sku : '-' }}</td>
                                <td><span class="text-success"></span>{{ $product->amount }}</td>
                                <td>{{ format_uang($product->salling_price) }}</td>
                                <td>
                                    @foreach ($product->product_categories as $category_product)
                                        <a href="{{ route('product_category.detail', $category_product->slug) }}" class="badge badge-primary">{{ $category_product->name }}</a>
                                    @endforeach
                                </td>
                                <td title="{{ !empty($product->created_at) ? tanggal_indonesia($product->created_at) : '-' }}">
                                    {{ !empty($product->created_at) ? date('d/m/Y', strtotime($product->created_at)) : '-' }}
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </body>
                @endtable
            </div>
        </div>            
            
        {{-- @if($products->count() >= 10)
            <div class="mt-3 float-right">
                {{ $products->links() }}
            </div>                
        @endif --}}
    </div>
</div>
@include('components.sweet')
@endsection


@push('scripts')
<script>
    let table = $('.table').DataTable({
        'autoWidth': false,
        "language": {
          "emptyTable": "Ups data masih kosong!.",
          "zeroRecords": "Ups data tidak ditemukan!."
        },
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "pageLength": 100
    })

    $('#select-all').click(function() {
        $(':checkbox').prop('checked', this.checked);
    })
    
    $('.name_product').each(function (e) {
        $(this).hover(function () {
            $(this).children().eq(1).toggleClass('d-none')
        })
    })

</script>
@endpush