@extends('layouts.master')

@section('title', 'Dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-6 col-md-3">
        <div class="small-box bg-indigo">
            <div class="inner">
                <h3>10</h3>
                <p>ORDER HARI INI</p>
            </div>
            <div class="icon">
                <i class="fas fa-cart-arrow-down"></i>
            </div>
            <a href="{{ url('admin/order') }}" class="small-box-footer">Lihat detail <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-indigo">
            <div class="inner">
                <h3>10</h3>
                <p>BELUM DIPROSES</p>
            </div>
            <div class="icon">
                <i class="fas fa-truck"></i>
            </div>
            <a href="{{ url('admin/order/unprocessed') }}" class="small-box-footer">Lihat detail <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-indigo">
            <div class="inner">
                <h3>10</h3>
                <p>PRODUK TERJUAL</p>
            </div>
            <div class="icon">
                <i class="fas fa-hand-holding-usd"></i>
            </div>
            <a href="{{ url('admin/report') }}" class="small-box-footer">Lihat detail <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-indigo">
            <div class="inner">
                <h3>10</h3>
                <p>GROSS PROFIT</p>
            </div>
            <div class="icon">
                <i class="fas fa-donate"></i>
            </div>
            <a href="{{ url('admin/report') }}" class="small-box-footer">Lihat detail <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card card-indigo card-outline">
            <div class="card-header border-bottom-0 p-0">
                <p class="text-center card-title">Grafik Penjualan </p>
                <p class="text-center">{{ tanggal_indonesia(date('Y-m-d')) }} s/d {{ tanggal_indonesia(date('Y-m-d')) }}</p>
                
            </div>
            <div class="card-body">
                <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChart" height="185" style="height: 185px;"></canvas>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-6">
                    <div class="description-block border-right">
                        <span class="description-percentage text-indigo"><i class="fas fa-caret-up"></i> 17%</span>
                        <h5 class="description-header">$35,210.43</h5>
                        <span class="description-text">TOTAL REVENUE</span>
                    </div>
                </div>
                <div class="col-sm-3 col-6">
                    <div class="description-block border-right">
                        <span class="description-percentage text-orange"><i class="fas fa-caret-left"></i> 0%</span>
                        <h5 class="description-header">$10,390.90</h5>
                        <span class="description-text">TOTAL COST</span>
                    </div>
                </div>
                <div class="col-sm-3 col-6">
                    <div class="description-block border-right">
                        <span class="description-percentage text-indigo"><i class="fas fa-caret-up"></i> 20%</span>
                        <h5 class="description-header">$24,813.53</h5>
                        <span class="description-text">TOTAL PROFIT</span>
                    </div>
                </div>
                <div class="col-sm-3 col-6">
                    <div class="description-block">
                        <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 18%</span>
                        <h5 class="description-header">1200</h5>
                        <span class="description-text">GOAL COMPLETIONS</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components.sweet')
@endsection

@push('scripts')
<script src="{{ asset('/AdminLTE/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('/AdminLTE/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('/AdminLTE/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('/AdminLTE/plugins/jquery-mapael/maps/world_countries.min.js') }}"></script>
<script src="{{ asset('/AdminLTE/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('/AdminLTE/plugins/moment/moment.min.js') }}"></script>
<script>
    
    $('.table').DataTable({
        'paginate': false,
        'searching': false,
        'bInfo': false,
        "language": {
            "emptyTable": "Ups data masih kosong!.",
            "zeroRecords": "Ups data tidak ditemukan!."
        },
    })

    $(function() {
        var salesChartCanvas = $('#salesChart').get(0).getContext("2d");
        var salesChartData = {
            labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label               : 'Digital Goods',
                    backgroundColor     : '#6610f2',
                    borderColor         : '#4600b9',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label               : 'Electronics',
                    backgroundColor     : 'rgba(210, 214, 222, .8)',
                    borderColor         : 'rgba(210, 214, 222, 1)',
                    pointRadius         : false,
                    pointColor          : 'rgba(210, 214, 222, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data                : [65, 59, 80, 81, 56, 55, 40]
                },
            ]
        }

        var salesChartOptions = {
            maintainAspectRatio : false,
            responsive : true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines : {
                        display : false,
                    }
                }],
                yAxes: [{
                    gridLines : {
                    display : false,
                    }
                }]
            }
        }

        var salesChart = new Chart(salesChartCanvas, { 
            type: 'line', 
            data: salesChartData, 
            options: salesChartOptions
        })
    })
</script>
@endpush