<?php

namespace App;

use App\Model;

class Product_category extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class)->withTimestamps();
    }
}
