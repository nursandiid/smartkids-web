<?php

namespace App;

use App\Model;

class Product extends Model
{

    public function product_stocks()
    {
        return $this->hasMany(Product_stock::class);
    }

    public function product_stock()
    {
        return $this->hasOne(Product_stock::class);
    }

    public function product_attributes()
    {
    	return $this->hasMany(Product_attribute::class);
    }

    public function product_categories()
    {
        return $this->belongsToMany(Product_category::class)->withTimestamps();
    }
}
