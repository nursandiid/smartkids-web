<?php

namespace App;

use App\Model;

class Product_stock extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
