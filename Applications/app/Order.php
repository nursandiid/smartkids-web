<?php

namespace App;

use App\Model;

class Order extends Model
{
    public function reseller()
    {
        return $this->belongsTo(Reseller::class);
    }
}
