<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
    	return view('outlets.index');
    }

    public function supplier()
    {
    	return view('suppliers.index');
    }

    public function user()
    {
    	return view('users.index');
    }

    public function saleTarget()
    {
    	return view('sales.index');
    }
}
