<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Billing_address;

use Validator;
use Auth;

class ApiAddressBillingController extends Controller
{
	public function __construct()
	{
		return auth()->shouldUse('reseller');
	}

    public function show()
    {
        $user = Auth::user()->id;
        $address = Billing_address::where('id_reseller', $user)->first();

        return response()->json(compact('address'), 200);
    }

    public function update(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'country' => 'required|min:3|max:30',
            'street_address' => 'required|min:3',
            'city' => 'required|min:3|max:30',
            'province' => 'required|min:3|max:30',
            'postal_code' => 'required|numeric|digits:5',
            'phone' => 'required|numeric',
            'email' => 'required|email'
        ]);

        // validation
        if ($validator->fails()) return response()->json($validator->errors(), 422);

        $user = Auth::user()->id;
        $address = Billing_address::where('id_reseller', $user)->first();

        $address->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_name' => $request->company_name,
            'country' => $request->country,
            'street_address' => $request->street_address,
            'apartement' => $request->apartment,
            'city' => $request->city,
            'province' => $request->province,
            'postal_code' => $request->postal_code,
            'phone' => $request->phone,
            'email' => $request->email
        ]);

        return response()->json([
            'message' => 'Alamat penagihan berhasil diperbaharui.'
        ], 201);
    }
}
