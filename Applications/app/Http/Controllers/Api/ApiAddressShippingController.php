<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shipping_address;

use Validator;
use Auth;

class ApiAddressShippingController extends Controller
{
   public function __construct()
	{
		return auth()->shouldUse('reseller');
	}

    public function show()
    {
        $user = Auth::user()->id;
        $address = Shipping_address::where('id_reseller', $user)->first();

        return response()->json(compact('address'));
    }

    public function update(Request $request)
    {
        $messages = [
            'digits' => 'Isian postal code harus berdigit 5 angka.'
        ];

    	$validator = Validator::make($request->all(), [
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'country' => 'required|min:3|max:50',
            'street_address' => 'required|min:3',
            'city' => 'required|min:3|max:30',
            'province' => 'required|min:3|max:30',
            'postal_code' => 'required|numeric|digits:5',
        ], $messages);

        // validation
        if ($validator->fails()) return response()->json($validator->errors(), 422);

        $user = Auth::user()->id;
        $address = Shipping_address::where('id_reseller', $user)->first();

        $address->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_name' => $request->company_name,
            'country' => $request->country,
            'street_address' => $request->street_address,
            'apartement' => $request->apartement,
            'city' => $request->city,
            'province' => $request->province,
            'postal_code' => $request->postal_code
        ]);

        return response()->json([
            'message' => 'Alamat pengiriman berhasil diperbaharui.'
        ], 201);
    }
}
