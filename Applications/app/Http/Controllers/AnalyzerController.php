<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Analyzer;

class AnalyzerController extends Controller
{
    public function index()
    {
    	# code...
    }

    public function show($id)
    {
    	# code...
    }

    public function create()
    {
    	# code...
    }

    public function store(Request $request)
    {
    	# code...
    }

    public function update(Analyzer $analyzer,  Request $request)
    {
    	# code...
    }

    public function destroy(Analyzer $analyzer)
    {
    	# code...
    }
}
