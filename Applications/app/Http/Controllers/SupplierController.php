<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        return view('suppliers.index');
    }

    public function listData()
    {
        $supplier = Supplier::latest()->get();
        $no = 0;
        $data = array();

        foreach ($supplier as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->name;
            $row[] = $list->delivery_location;
            $row[] = $list->phone;
            $row[] = $list->address;
            $row[] = '
                    <a href="#" onclick="edit('. $list->id .')" class="btn btn-link"><i class="fas fa-pencil-alt"></i></a>
                    <a href="#" onclick="_delete('. $list->id .')" class="btn btn-link text-danger"><i class="fas fa-trash-alt"></i></a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function store(Request $request)
    {
        $supplier = Supplier::create([
            'name' => $request->name,
            'delivery_location' => $request->delivery_location,
            'phone' => $request->phone,
            'address' => $request->address,
            'description' => $request->description
        ]);

        return response()->json([
            'message' => 'Supplier berhasil ditambahkan.'
        ]);
    }

    public function edit(Supplier $supplier)
    {
        echo json_encode($supplier);
    }

    public function update(Request $request, Supplier $supplier)
    {
        $supplier->update([
            'name' => $request->name,
            'delivery_location' => $request->delivery_location,
            'phone' => $request->phone,
            'address' => $request->address,
            'description' => $request->description
        ]);

        return response()->json([
            'message' => 'Supplier berhasil diubah.'
        ]);
    }

    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
        return response()->json([
            'message' => 'Data supplier berhasil dihapus.'
        ]);
    }
}
