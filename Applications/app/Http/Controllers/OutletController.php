<?php

namespace App\Http\Controllers;

use App\Outlet;
use Illuminate\Http\Request;
use Image;
use Storage;
use Carbon\Carbon;

class OutletController extends Controller
{

    public function edit($id)
    {
        $outlet = Outlet::find($id);
        echo json_encode($outlet);
    }

    public function _saveFile($name, $image)
    {
        $name = str_slug($name).'-'. Carbon::now()->toDateTimeString() .'.'. $image->getClientOriginalExtension();

        if (!Storage::disk('public')->exists('uploads/outlets')) {
            Storage::disk('public')->makeDirectory('uploads/outlets');
        }
        
        $image = Image::make($image)->save();
        Storage::disk('public')->put('uploads/outlets/'. $name, $image);
        
        return $name;
    }

    public function insert(Request $request)
    {
        $view_outlet = Outlet::where('id', $request->id)->first();
        if (!empty($view_outlet) == 0) {
            $outlet = new Outlet;
            $outlet->name    = $request->name;
            $outlet->slug    = str_slug($request->name);
            $outlet->address = $request->address;
            $outlet->phone   = $request->phone;

            if($request->hasFile('logo')) {
                $logo = $this->_saveFile('logo', $request->file('logo'));
                $outlet->logo = $logo;
            }
            $outlet->save();
        } else {
            $outlet = Outlet::find($request->id);
            $outlet->name    = $request->name;
            $outlet->slug    = str_slug($request->name);
            $outlet->address = $request->address;
            $outlet->phone   = $request->phone;

            if($request->hasFile('logo')) {
                $logo = $this->_saveFile('logo', $request->file('logo'));
                $outlet->logo = $logo;
            }
            $outlet->update();
        }

        return response()->json([
            'message' => 'Logo & nama toko berhasil diubah.'
        ]);

    }
}
