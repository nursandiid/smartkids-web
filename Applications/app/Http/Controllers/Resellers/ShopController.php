<?php

namespace App\Http\Controllers\Resellers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product_category;
use App\Product;

class ShopController extends Controller
{
    private $paginate = 15;

    public function index(Request $request)
    {
        $products = Product::latest('created_at')->paginate($this->paginate);
        $categories = Product_category::all();

        if ($request->ajax()) {
            return view('resellers.partials.paginate', compact('products'))->render();
        }

        return view('resellers.shop.shop', compact('products', 'categories'));
    }

    public function show(Request $request)
    {
        $product = Product::with('product_category', 'product_stock')->where('id', $request->id)->first();
        
        return view('resellers.shop.detail', compact('product'));
    }

    public function search(Request $request)
    {
        $search = $request->get('q');
        $products = Product::where('name', 'LIKE', '%' . "$search" . '%')
                            ->latest('created_at')->paginate($this->paginate);
        $products->appends(['q' => $search]);
        $categories = Product_category::all();
        
        if($products != null) {
            if ($request->ajax()) {
                return view('resellers.partials.paginate', compact('products'))->render();
            }

            return view('resellers.shop.shop', compact('products', 'categories'));
        }else{
            $output .= '<h2 class="pt-4 m-auto">Produk tidak ditemukan</h2>';

            return response()->json($output, 200);
        }
    }

    public function sort(Request $request)
    {
        if($request->id == "0") {
            $products = new Product();
        }else{
            $products = Product::where('product_category_id', $request->id);
        }

        $categories = Product_category::all();

        switch ($request->key) {
            // new
            case 1:
                $products = $products->latest('created_at')->paginate($this->paginate);
                break;
            // latest
            case 2:
                $products = $products->paginate($this->paginate);
                break;
            // cheap
            case 3:
                $products = $products->orderBy('reseller_price', 'ASC')->paginate($this->paginate);
                break;
            // expensive
            case 4:
                $products = $products->orderBy('reseller_price', 'DESC')->paginate($this->paginate);
                break;
            // A-Z
            case 5:
                $products = $products->orderBy('name', 'ASC')->paginate($this->paginate);
                break;
            // Z-A
            case 6:
                $products = $products->orderBy('name', 'DESC')->paginate($this->paginate);
                break;
            
            default:
                $products = null;
                break;
        }
        
        $products->appends(['id' => $request->id, 'key' => $request->key]);
        if ($request->ajax()) {
            return view('resellers.partials.paginate', compact('products', 'categories'))->render();
        }

        return view('resellers.shop.shop', compact('products', 'categories'));
    }
}
