<?php

namespace App\Http\Controllers\Resellers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Order;

use Auth;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with('reseller')->where('reseller_id', Auth::guard('reseller')->user()->id)->get();
        
        return view('resellers.order.order', compact('orders'));
    }
}
