<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_attribute;
use App\Product;

class ProductAttributeController extends Controller
{
    public function index()
    {
        $products   = Product::latest()->get();
        $attributes = Product_attribute::with('product')->latest()->get();

    	return view('product_attributes.index', compact('products', 'attributes'));
    }

    public function edit($id)
    {
        $attribute = Product_attribute::find($id);
        echo json_encode($attribute);
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
            'name' => 'required|string',
            'product_id' => 'required|integer'
        ]);

        try {
            $attribute = Product_attribute::create(request()->all());
            return back()->with('message', [
                'title' => 'Atribut berhasil ditambahkan.'
            ]);

        } catch (Exception $e) {
            return back()->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(), [
            'name' => 'required|string',
            'product_id' => 'required|integer'
        ]);

        try {
            $attribute = Product_attribute::find($id)->update(request()->all());
            return redirect()->route('product_attribute.index')->with('message', [
                'title' => 'Atribut berhasil diubah.'
            ]);
        } catch (Exception $e) {
            return redirect()->route('product_attribute.index')->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function destroy($id)
    {
    	$attribute = Product_attribute::find($id)->delete();
        return back()->with('message', [
            'title' => 'Atribut berhasil dihapus.'
        ]);
    }
}
