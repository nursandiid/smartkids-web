<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id_1')->comment('Customer')->unsigned();
            $table->integer('customer_id_2')->comment('Sent to')->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->dateTime('date_of_order');
            $table->text('note')->nullable();
            $table->integer('total_item');
            $table->integer('total_price');
            $table->integer('discount')->nullable()->default(0);
            $table->integer('price');
            $table->integer('be_accepted');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
