-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 08 Jan 2020 pada 08.24
-- Versi Server: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `afsalesi_smartkids`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `attribute_provisions`
--

CREATE TABLE `attribute_provisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `billing_addresses`
--

CREATE TABLE `billing_addresses` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_reseller` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `street_address` text NOT NULL,
  `apartement` varchar(191) DEFAULT NULL,
  `city` varchar(191) NOT NULL,
  `province` varchar(191) DEFAULT NULL,
  `postal_code` int(5) DEFAULT NULL,
  `phone` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `billing_addresses`
--

INSERT INTO `billing_addresses` (`id`, `id_reseller`, `first_name`, `last_name`, `company_name`, `country`, `street_address`, `apartement`, `city`, `province`, `postal_code`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(1, 1, 'Roby', 'Pangestu', NULL, 'Albania', 'Ngemplak nganti, Karang Geneng, Sendangadi, Kec. Mlati, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55285', NULL, 'Yogyakarta', 'Test', 54123, '08987271288', 'test@mail.com', '2019-12-28 06:51:55', '2020-01-06 09:01:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `carts`
--

CREATE TABLE `carts` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_reseller` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_category` enum('pelanggan','reseller','dropshipper') COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`id`, `name`, `customer_category`, `province`, `district`, `sub_district`, `postal_code`, `phone`, `email`, `other_contact`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Test1', 'dropshipper', '-', '-', '-', 134334, '08349384838', 'test@mail.com', '-', '-', '2020-01-07 02:02:18', '2020-01-07 02:02:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `expenses`
--

INSERT INTO `expenses` (`id`, `date`, `name`, `price`, `amount`, `subtotal`, `note`, `created_at`, `updated_at`) VALUES
(1, '05/01/2020', 'test', 200000, 2, 400000, '-', '2020-01-07 02:03:15', '2020-01-07 02:03:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_09_01_111100_create_password_resets_table', 1),
(2, '2019_10_14_093700_create_roles_table', 1),
(3, '2019_10_14_093701_create_users_table', 1),
(4, '2019_10_14_093710_create_product_categories_table', 1),
(5, '2019_10_14_094838_create_products_table', 1),
(6, '2019_10_14_094911_create_product_stocks_table', 1),
(7, '2019_10_14_094931_create_orders_table', 1),
(8, '2019_10_14_094950_create_customers_table', 1),
(9, '2019_10_14_095007_create_expenses_table', 1),
(10, '2019_10_16_230319_create_outlets_table', 1),
(11, '2019_10_16_230734_create_suppliers_table', 1),
(12, '2019_10_16_232040_create_permissions_table', 1),
(13, '2019_10_16_232126_create_role_has_permissions_table', 1),
(14, '2019_10_27_074311_create_variant_products_table', 1),
(15, '2019_12_15_135216_create_product_attributes_table', 2),
(16, '2019_12_15_135834_create_attribute_provisions_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `reseller_id` int(10) UNSIGNED NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `total_item` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `discount` int(11) DEFAULT '0',
  `status` enum('Pending Payment','On Hold','Completed','Canceled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending Payment',
  `be_accepted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `reseller_id`, `note`, `total_item`, `total_price`, `discount`, `status`, `be_accepted`, `created_at`, `updated_at`) VALUES
(14, 1, NULL, 1, 2000000, 0, 'On Hold', 0, '2020-01-06 10:28:19', '2020-01-06 10:28:19'),
(15, 1, NULL, 2, 4000000, 0, 'Pending Payment', 0, '2020-01-06 11:46:51', '2020-01-06 11:46:51'),
(16, 1, NULL, 2, 4000000, 0, 'On Hold', 0, '2020-01-06 11:47:05', '2020-01-06 11:51:59'),
(17, 1, NULL, 3, 6000000, 0, 'Pending Payment', 0, '2020-01-06 11:57:46', '2020-01-06 11:57:46'),
(18, 1, NULL, 3, 6000000, 0, 'Pending Payment', 0, '2020-01-06 11:58:27', '2020-01-06 11:58:27'),
(19, 1, NULL, 1, 2000000, 0, 'Pending Payment', 0, '2020-01-06 21:26:08', '2020-01-06 21:26:08'),
(20, 1, NULL, 1, 2000000, 0, 'Pending Payment', 0, '2020-01-06 21:28:49', '2020-01-06 21:28:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `outlets`
--

CREATE TABLE `outlets` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `outlets`
--

INSERT INTO `outlets` (`id`, `logo`, `slug`, `name`, `phone`, `address`, `description`, `created_at`, `updated_at`) VALUES
(1, 'logo-2019-11-01 06:40:32.jpg', 'majalengka-menjamu', 'Majalengka Menjamu', '081383886575', 'Lingkungan Mekar Jaya No 20, RT 007 RW 003, Kelurahan Tonjong - Majalengka', NULL, '2019-10-31 23:40:23', '2019-11-03 00:26:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `id_reseller` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `amount` int(11) NOT NULL,
  `bank_name` varchar(20) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `payments`
--

INSERT INTO `payments` (`id`, `id_reseller`, `order_id`, `name`, `amount`, `bank_name`, `photo`, `created_at`, `updated_at`) VALUES
(1, 1, 14, 'Agung', 2000000, 'BCA', 'agung-2020-01-06 18:16:28.jpg', '2020-01-06 11:16:28', '2020-01-06 11:16:28'),
(2, 1, 16, 'Nova', 2000000, 'BRI SYARIAH', 'nova-2020-01-06 18:49:06.jpg', '2020-01-06 11:49:06', '2020-01-06 11:49:06'),
(3, 1, 16, 'Nova', 2000000, 'BRI SYARIAH', 'nova-2020-01-06 18:49:27.jpg', '2020-01-06 11:49:27', '2020-01-06 11:49:27'),
(4, 1, 16, 'admin', 2000000, 'BRI SYARIAH', 'admin-2020-01-06 18:51:59.jpg', '2020-01-06 11:51:59', '2020-01-06 11:51:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Add Order', '2019-10-31 20:25:47', '2019-10-31 20:25:47'),
(2, 'Edit Order', '2019-10-31 20:33:25', '2019-10-31 20:33:25'),
(3, 'Delete Order', '2019-10-31 20:33:29', '2019-10-31 20:33:29'),
(4, 'View Order', '2019-10-31 20:34:44', '2019-10-31 20:34:44'),
(5, 'View Product', '2019-10-31 23:12:08', '2019-10-31 23:12:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `normal_price` int(11) DEFAULT NULL,
  `salling_price` int(11) DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Like code or anything!',
  `manage_stock` tinyint(1) NOT NULL DEFAULT '1',
  `amount` int(10) NOT NULL,
  `allow_backorder` enum('Tidak diizinkan','Diizinkan, tetapi beritahu customer','Diizinkan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `limit_stock` int(10) NOT NULL,
  `sold_individually` tinyint(1) NOT NULL DEFAULT '1',
  `weight` int(11) DEFAULT NULL,
  `length` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `height` int(10) DEFAULT NULL,
  `shipping_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `normal_price`, `salling_price`, `sku`, `manage_stock`, `amount`, `allow_backorder`, `limit_stock`, `sold_individually`, `weight`, `length`, `width`, `height`, `shipping_class`, `photo`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'WWP SERI 1&2/3&4 Tebus Camera', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(3, '2 Azalea diskon promo 50%', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(4, 'Food Processor', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(5, 'SUPER KIDS 1', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(6, 'FULL Package English Time', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(7, 'English Time Baby Pack BONUS Ressa', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(8, 'WWP CPD BILINGUAL', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(9, 'Mushaf Maqamat for Kids Tebus Hafiz Hafizah Junior', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(10, 'Smart Cooker', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(11, 'Remote Hafiz Doll Bilingual', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(12, 'Mixer', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(13, 'Buku Edukasi Bunda Ajarkan Aku Ibadah ETL', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(14, 'Pressure Cooker', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(15, 'Vienta Paket Hemat 2', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(16, 'Double Pan', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(17, 'ANAK BIRU ? IGLASSES KHUSUS RESELLER', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(18, 'CAKRAWALA PENGETAHUAN DASAR TEBUS ETBP 1 JT', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(19, 'BABY FOOD MAKER', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(20, 'Smart Pressure Cooker Tebus SH 1jt', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(21, 'FOOD PROCESSOR TRITAN TEBUS SMART HAFIZ + SP', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(22, '(PROMO MERDEKA) BABY FOOD MAKER', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(23, 'Smart Pressure Cooker', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(24, 'BABY FOOD MAKER FREE 1pcs BABY RATTLE', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(25, 'DISKON 50rb SMART HAFIZ V 3 FREE TAS', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(26, 'PROMO BUNDLING HAFIZ BABY RATTLE BLUE AND PINK', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(27, 'MM Kids Tanpa Al Hafiz', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(28, 'New Smart Hafiz Touch Screen Tebus Cute Camera', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(29, 'PROMO AL HAFIZ + CHIP', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(30, 'SMART HAFIZ VERSI ? 1+SP TEBUS HAFIZAH DOLL', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(31, 'Chip Smart Hafiz', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(32, 'New Smart Hafiz', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(33, 'DUA BOUGENVILLE diskon 50%', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(34, 'LITTLE ABID TEBUS MURAH EPEN HAFIZ', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(35, 'MIMO Tebus Smart Hafiz', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(36, 'BAAI ETL EPEN', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(37, '(PROMO MERDEKA) ROCKET WALTER', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(38, 'WIDYA WIYATA PERTAMA', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(39, 'Hafiz/Hafizah Talking Doll New Edition', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(40, 'Boneka Edukasi HafiZ Talking Doll', NULL, NULL, NULL, '', 1, 0, 'Tidak diizinkan', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(41, 'Test', '<p>Test</p>', 23232, 2323, 'SDFDFD', 1, 12, 'Diizinkan, tetapi beritahu customer', 5, 1, 1, 10, 10, 10, 'J&T', 'test-2020-01-08 00:13:46.png', 1, '2020-01-07 08:06:25', '2020-01-07 17:13:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `name`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'Warna', 28, '2020-01-07 02:38:07', '2020-01-07 02:38:07'),
(2, 'Ukuran', 41, '2020-01-07 14:11:42', '2020-01-07 14:11:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Makanan', 'makanan', '-', '2019-10-30 13:27:38', '2019-10-30 13:27:38'),
(2, 'Pakaian', 'pakaian', '-', '2019-10-30 13:27:47', '2019-10-30 13:27:47'),
(3, 'Mainan Ringan', 'mainan-ringan', '-', '2019-11-18 02:43:15', '2020-01-07 08:54:31'),
(4, 'Nova', 'nova', '123', '2019-12-15 07:42:47', '2019-12-15 07:42:47'),
(5, 'Jelita test', 'jelita-test', '-', '2020-01-07 18:08:02', '2020-01-07 18:08:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_product_category`
--

CREATE TABLE `product_product_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_product_category`
--

INSERT INTO `product_product_category` (`id`, `product_id`, `product_category_id`, `created_at`, `updated_at`) VALUES
(1, 41, 4, NULL, NULL),
(2, 41, 3, NULL, NULL),
(3, 41, 2, NULL, NULL),
(4, 41, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_stocks`
--

CREATE TABLE `product_stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_changed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `product_stocks`
--

INSERT INTO `product_stocks` (`id`, `date_changed`, `product_id`, `amount`, `description`, `created_at`, `updated_at`) VALUES
(4, '', 1, 3, NULL, NULL, NULL),
(5, '08/01/2020', 2, 4, '-', '0000-00-00 00:00:00', '2020-01-07 14:03:08'),
(6, '01/01/2020', 3, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:07:42'),
(7, '07/12/2019', 4, 78, '-', '0000-00-00 00:00:00', '2020-01-07 14:07:49'),
(8, '04/12/2019', 5, 94, '-', '0000-00-00 00:00:00', '2020-01-07 14:07:55'),
(9, '03/12/2019', 6, 100, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:07'),
(10, '02/12/2019', 7, 99, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:40'),
(11, '20/12/2019', 8, 100, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:13'),
(12, '10/12/2019', 9, 89, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:27'),
(13, '24/12/2019', 10, 90, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:46'),
(14, '18/12/2019', 11, 97, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:21'),
(15, '03/01/2020', 12, 88, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:59'),
(16, '01/01/2020', 13, 94, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:51'),
(17, '02/01/2020', 14, 99, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:54'),
(18, '04/01/2020', 15, 97, '-', '0000-00-00 00:00:00', '2020-01-07 14:10:03'),
(19, '05/01/2020', 16, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:10:08'),
(20, '17/12/2019', 17, 4, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:46'),
(21, '07/01/2020', 18, 4, '-', '0000-00-00 00:00:00', '2020-01-07 14:10:12'),
(22, '', 19, 2, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '21/12/2019', 20, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:55'),
(24, '15/12/2019', 21, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:39'),
(25, '', 22, 9, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '', 23, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '23/12/2019', 24, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:05'),
(28, '18/12/2019', 25, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:08:33'),
(29, '', 26, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '', 27, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '', 28, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '25/12/2019', 29, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:00'),
(33, '09/12/2019', 30, 4, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:26'),
(34, '', 31, 0, '-', '0000-00-00 00:00:00', '2020-01-07 13:59:55'),
(35, '', 32, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '30/12/2019', 33, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:11'),
(37, '', 34, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '', 35, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '17/12/2019', 36, 3, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:21'),
(40, '', 37, 10, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '16/12/2019', 38, 0, '-', '0000-00-00 00:00:00', '2020-01-07 14:09:16'),
(42, '', 39, 98, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '', 40, 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '08/01/2020', 41, 12, 'Tambah produk baru', '2020-01-07 16:34:52', '2020-01-07 17:13:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `resellers`
--

CREATE TABLE `resellers` (
  `id` int(11) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 belum aktif',
  `username` varchar(30) NOT NULL,
  `email` varchar(191) NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(191) NOT NULL,
  `postal_code` varchar(191) DEFAULT NULL,
  `phone` varchar(191) NOT NULL,
  `bank_name` varchar(191) NOT NULL,
  `bank_account` varchar(191) NOT NULL,
  `bank_account_name` varchar(191) NOT NULL,
  `ktp` varchar(191) DEFAULT NULL,
  `partners_name` varchar(191) NOT NULL,
  `office_address` text,
  `phone_recruiter` varchar(191) NOT NULL,
  `email_recruiter` varchar(191) DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `resellers`
--

INSERT INTO `resellers` (`id`, `active`, `username`, `email`, `first_name`, `last_name`, `password`, `address`, `city`, `postal_code`, `phone`, `bank_name`, `bank_account`, `bank_account_name`, `ktp`, `partners_name`, `office_address`, `phone_recruiter`, `email_recruiter`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', 'test@mail.com', 'testi', 'test', '$2y$10$XG4U97JHIo3QJxW.VvWoH.N24yDQgLVp94bQbnXZxmJnxwme.xJEW', 'Ngemplak', 'Yogyakarta', '54123', '08987271288', 'BRI', '1221212211', 'test', 'test-test-2019-12-28 13:51:54.png', 'test', 'test', '086388580446', NULL, 'rf29tHFCHLurEiZzdlNzue2uuIhpvh8OfOkTpJHv8uWXlovaSNNC4MWdpAeY', '2019-12-28 06:51:55', '2020-01-05 23:04:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2019-10-31 19:37:31', '2019-10-31 19:37:31'),
(2, 'CS4', '2019-10-31 19:46:44', '2020-01-07 14:21:19'),
(3, 'CS3', '2019-11-01 18:04:09', '2020-01-07 14:21:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(24, 1, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(25, 2, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(26, 3, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(27, 4, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(28, 5, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(29, 1, 2, '2019-10-31 23:18:00', '2019-10-31 23:18:00'),
(30, 2, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01'),
(31, 3, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01'),
(32, 4, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01'),
(33, 5, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `shipping_addresses`
--

CREATE TABLE `shipping_addresses` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_reseller` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `street_address` text NOT NULL,
  `apartement` varchar(191) DEFAULT NULL,
  `city` varchar(191) NOT NULL,
  `province` varchar(191) DEFAULT NULL,
  `postal_code` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `shipping_addresses`
--

INSERT INTO `shipping_addresses` (`id`, `id_reseller`, `first_name`, `last_name`, `company_name`, `country`, `street_address`, `apartement`, `city`, `province`, `postal_code`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', 'test', NULL, NULL, 'Ngemplak', NULL, 'Yogyakarta', NULL, 54123, '2019-12-28 06:51:55', '2019-12-28 06:51:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `delivery_location`, `phone`, `address`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Test1', 'Klangenan', '081234555789', '-', '-', '2020-01-07 01:57:24', '2020-01-07 01:57:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `photo`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin@afsales.com', NULL, '$2y$10$vRwcCUopFt4Bf6gjMQRPXe9MYyjohSiO3jf3FHbIIMcCpQ6s35PzS', 'administrator-2019-11-21 04:38:26.png', 1, 'k1XhpRDutRqIYoYal27XPqHpOAbzmII75aoRaT39mYqpbhmBx7FNqlAP8KjH', '2019-10-29 21:41:40', '2019-12-15 10:00:25'),
(2, 'User', 'user', 'user@afsales.com', NULL, '$2y$10$TxTnaIPv1D41.NNWCehpLuEMCr8HUq9lt9Wj8yfFpDKm3dsqVXoIK', 'user.png', 2, 'dx94HaiubWNR4O5wpKfBZLrt2yh7ljm63xwiKzn0qi5ilii51hvL5OFAcMhl', '2019-10-29 21:41:41', '2019-10-29 21:41:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `variant_products`
--

CREATE TABLE `variant_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attribute_provisions`
--
ALTER TABLE `attribute_provisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_reseller` (`id_reseller`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_ibfk_1` (`id_product`),
  ADD KEY `id_reseller` (`id_reseller`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reseller_id` (`reseller_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `order_product` (`product_id`);

--
-- Indexes for table `outlets`
--
ALTER TABLE `outlets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_reseller` (`id_reseller`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_product_category`
--
ALTER TABLE `product_product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resellers`
--
ALTER TABLE `resellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_reseller` (`id_reseller`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variant_products`
--
ALTER TABLE `variant_products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attribute_provisions`
--
ALTER TABLE `attribute_provisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `outlets`
--
ALTER TABLE `outlets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_product_category`
--
ALTER TABLE `product_product_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product_stocks`
--
ALTER TABLE `product_stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `resellers`
--
ALTER TABLE `resellers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `variant_products`
--
ALTER TABLE `variant_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `billing_addresses`
--
ALTER TABLE `billing_addresses`
  ADD CONSTRAINT `billing_addresses_ibfk_1` FOREIGN KEY (`id_reseller`) REFERENCES `billing_addresses` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`id_reseller`) REFERENCES `resellers` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`reseller_id`) REFERENCES `resellers` (`id`);

--
-- Ketidakleluasaan untuk tabel `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`id_reseller`) REFERENCES `resellers` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD CONSTRAINT `shipping_addresses_ibfk_1` FOREIGN KEY (`id_reseller`) REFERENCES `resellers` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
